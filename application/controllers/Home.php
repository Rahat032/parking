<?php

class Home extends CI_Controller
{
        function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url','language'));
                $this->load->model('ion_auth_model');
                $this->load->model('spot_model');

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
	}
        public function index()
	{
            if ($this->session->userdata('user_id')) {
                $this->data["user"] = $this->ion_auth->user()->row();
                $data['active'] = 1;
                $this->_render_page('account', $data);
                
            }
            else{
                $this->load->view('index.php');
            }
	}
        public function terms()
	{
            
            if ($this->session->userdata('user_id')) {
                $this->data["user"] = $this->ion_auth->user()->row();
                $data['active'] = 1;
                $data['loginValid'] = 1;
               
                $this->load->view('terms', $data);
                
            }
            else{
                $this->load->view('terms');
            }
	}
        public function privacy()
	{
            
            if ($this->session->userdata('user_id')) {
                $this->data["user"] = $this->ion_auth->user()->row();
                $data['active'] = 1;
                $data['loginValid'] = 1;
               
                $this->load->view('privacy', $data);
                
            }
            else{
                $this->load->view('privacy');
            }
	}
        public function faq()
	{
            
            if ($this->session->userdata('user_id')) {
                $this->data["user"] = $this->ion_auth->user()->row();
                $data['active'] = 1;
                $data['loginValid'] = 1;
               
                $this->load->view('faq', $data);
                
            }
            else{
                $this->load->view('faq');
            }
	}
        function _render_page($view, $data=null, $returnhtml=false)//I think this makes more sense
	{

		$this->viewdata = (empty($data)) ? $this->data: $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);

		if ($returnhtml) return $view_html;//This will return html on 3rd argument being true
	}

}
?>