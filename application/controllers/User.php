<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
         * 
	 */
        function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url','language'));
                $this->load->model('ion_auth_model');
                $this->load->model('spot_model');
                $this->load->config('app', TRUE);

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
                //$this->lang->load('parking');
	}
	public function index()
	{
            if ($this->session->userdata('user_id')) {
               redirect("user/account");                 
            }
            else{
                $this->load->view('index.php');
            }
	}
        public function user_profile(){
            if ($this->session->userdata('user_id')) {
                       $userId=$this->session->userdata('user_id');
                       $userData = $this->ion_auth_model->get($userId);
                       $data['userData'] = $userData;
                       //var_dump($userData);exit();
                       //$content = $this->load->view('profile.php', $data, true);
                       $this->_render_page('profile.php',$data);
                       
	    }
            else{
                $this->load->view('login.php');
            }
	}
        
        public function addspot(){
            if ($this->session->userdata('user_id')) {
                    if($this->input->post()){
                        
                       
                        $checkH = $this->input->post('checkH',true);
                        $checkD = $this->input->post('checkD',true);
                        $checkW = $this->input->post('checkW',true);
                        $checkM = $this->input->post('checkM',true);
                        
                        if($checkH=="on"){
                            $valueH=$this->input->post('valueH', true);
                        } else{
                           $valueH=null;
                        }
                        if($checkD=="on"){
                            $valueD=$this->input->post('valueD', true);
                        } else{
                            $valueD=null;
                        }
                        if($checkW=="on"){
                            $valueW=$this->input->post('valueW', true);
                        } else{
                           $valueW=null;
                        }
                        if($checkM=="on"){
                             $valueM=$this->input->post('valueM', true);
                        } else{
                            $valueM=null;
                        }
                       
                        
                        $table = 'spot';

                          $config['upload_path'] = './assets/image';
                          $config['allowed_types'] = 'gif|jpg|png';
                          $config['max_size']	= $this->config->item('max_size', 'app');
                          $config['max_width']  = $this->config->item('max_width', 'app');
                          $config['max_height']  = $this->config->item('max_height', 'app');

                          $this->load->library('upload', $config);
                          if (!empty($_FILES['image']['name'])) {
                                if ( ! $this->upload->do_upload('image'))
                                {
                                        $error = array('error' => $this->upload->display_errors());
                                       $this->session->set_flashdata('message', "Error uploading image please check image size");
                                        $this->load->view('addspot.php');
                                }
                                else
                                {
                                        $data = array('upload_data' => $this->upload->data());
                                        $image = $data['upload_data']['file_name'];
                                }

                            } else{
                                 $image = null;
                            }
                          $user = $this->ion_auth->user()->row();
                          $userId= $user->id;

                      
                          
                      $nw_record_array = array(
                              'user_id' => $userId,
                              'address' => $this->input->post('address', true),
                             'spot_discription' => $this->input->post('spot_discription', true),
                             'image' => $image,
                              'spot_type' => $this->input->post('spot_type', true),
                              'spot_available_service' => $this->input->post('spot_available_service', true),
                              
                              'dateFrom'=>  $this->input->post('from', true),
                              'dateTo'=> $this->input->post('to', true),
                              'active' => 1,
                              'latitude'=>  $this->input->post('latitude', true),
                              'longitude'=> $this->input->post('longitude', true),
                              'hourlyPrice'=> $valueH,
                              'dailyPrice'=>  $valueD,
                              'weeklyPrice'=> $valueW,
                              'monthlyPrice'=> $valueM

                      );
                         $nw_record_id = $this->spot_model->add_nw_record($table, $nw_record_array);
                         redirect("user/list_spot");
                    }
                    else{
                          $data['active'] = 2;
                          $data['spot_price'] = $this->spot_model->get_price();  
                          $data['spot_type'] = $this->spot_model->get_type(); 
                          $data['spot_available_services'] = $this->spot_model->get_available_services();  
                          $data['max_size']   = $this->config->item('max_size', 'app');
                          $data['max_width']  =   $this->config->item('max_width', 'app');
                          $data['max_height'] = $this->config->item('max_height', 'app');
                          $data['zoom'] = $this->config->item('map_zoom', 'app');
                        $this->load->view('addspot.php',$data);
                    }
            }
            else{
                $this->load->view('login.php');
            }
	}
        public function list_spot()
        {
            if ($this->session->userdata('user_id')) {
               $table = 'spot';
               $user = $this->ion_auth->user()->row();
               $userId= $user->id;
               $_data_array['record_list'] = $this->spot_model->get_spotlist($table,$userId);
               $_data_array['active'] = 3;
               $this->load->view('spotList', $_data_array);
                
            }
            else{
                $this->load->view('index.php');
            }
            
             
        }
        public function edit_spot($Id) {
		if ($Id != '') {
                     $table = 'spot';
		    $_data_array['record_list'] = $this->spot_model->get_spot_details($table,$Id);
                    $_data_array['spot_price'] = $this->spot_model->get_price();  
                    $_data_array['spot_type'] = $this->spot_model->get_type(); 
                    $_data_array['spot_available_services'] = $this->spot_model->get_available_services();  
                    $_data_array['max_size']	= $this->config->item('max_size', 'app');
                    $_data_array['max_width']  = $this->config->item('max_width', 'app');
                    $_data_array['max_height']  = $this->config->item('max_height', 'app');
                    $_data_array['zoom'] = $this->config->item('map_zoom', 'app');
		    $this->load->view('spot_edit', $_data_array);
		} else {
                    redirect("user/list_spot");
		}
	}
        public function status($Id) {
		if ($Id != '') {
                     $table = 'spot';
		    $status= $this->spot_model->get_spot_details($table,$Id);
                    if($status[0]['active']==1)
                    {
                        $active=0;
                    }else{
                         $active=1;
                    }
                    $update_status = array(
                        'active' => $active
                    );
                    $this->spot_model->update_status($Id,$update_status);
                    
		   redirect("user/list_spot");
		} else {
                    redirect("user/list_spot");
		}
	}
        public function delete_spot($Id) {
		
		if ($Id > 0) {
			$this->spot_model->delete_spot($Id);
                        redirect("user/list_spot");
		} 

		redirect("user/list_spot");
	}
        public function update_spot($Id)
	{
            if ($Id != '') {
                    $table = 'spot';
                    $image=null;
                    $config['upload_path'] = './assets/image';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size']	= $this->config->item('max_size', 'app');
                    $config['max_width']  = $this->config->item('max_width', 'app');
                    $config['max_height']  = $this->config->item('max_height', 'app');

                    $this->load->library('upload', $config);
                    
                     $checkH = $this->input->post('checkH',true);
                     $checkD = $this->input->post('checkD',true);
                     $checkW = $this->input->post('checkW',true);
                     $checkM = $this->input->post('checkM',true);
                        
                    
                    if($checkH=="on"){
                            $valueH=$this->input->post('valueH', true);
                        } else{
                           $valueH=null;
                        }
                        if($checkD=="on"){
                            $valueD=$this->input->post('valueD', true);
                        } else{
                            $valueD=null;
                        }
                        if($checkW=="on"){
                            $valueW=$this->input->post('valueW', true);
                        } else{
                           $valueW=null;
                        }
                        if($checkM=="on"){
                             $valueM=$this->input->post('valueM', true);
                        } else{
                            $valueM=null;
                        }
                       

                    if ( ! $this->upload->do_upload('image'))
                    {
                            $error = array('error' => $this->upload->display_errors());
                            $this->session->set_flashdata('message', "Error uploading image please check image size");
                            $table = 'spot';
                            $_data_array['record_list'] = $this->spot_model->get_spot_details($table,$Id);
                            $_data_array['max_width']  = $this->lang->line('max_width');
                            $_data_array['max_height']  = $this->lang->line('max_height');
                            $this->load->view('spot_edit', $_data_array);
                    }
                    else
                    {
                            $data = array('upload_data' => $this->upload->data());
                            $image = $data['upload_data']['file_name'];
                    }
                    
                    
                    $user = $this->ion_auth->user()->row();
                    $userId= $user->id;
               
                
		$nw_record_array = array(
                        'user_id' => $userId,
			'address' => $this->input->post('address', true),
                        'spot_discription' => $this->input->post('spot_discription', true),
			'image' => $image,
			'spot_type' => $this->input->post('spot_type', true),
			'spot_available_service' => $this->input->post('spot_available_service', true),
			
			'dateFrom'=>  $this->input->post('from', true),
			'dateTo'=> $this->input->post('to', true),
                        'latitude'=>  $this->input->post('latitude', true),
                        'longitude'=> $this->input->post('longitude', true),
                        'hourlyPrice'=> $valueH,
                        'dailyPrice'=>  $valueD,
                        'weeklyPrice'=> $valueW,
                        'monthlyPrice'=> $valueM
		);
		$nw_record_id = $this->spot_model->update_spot($table, $nw_record_array, $Id);

                redirect("user/list_spot");
            } else {
                redirect("user/list_spot");
            }
		
	}
        public function account(){
            if ($this->session->userdata('user_id')) {
                $table="users";$Id= $this->session->userdata('user_id');
                $user = $this->ion_auth->user()->row();
                
                $this->session->set_userdata('fname',$user->first_name);
                $this->session->set_userdata('lname',$user->last_name);
              
                $_data_array['record_list'] = $this->spot_model->get_user($table,$Id);
                $_data_array['active'] = 1;
                $_data_array['max_size']	= $this->config->item('max_size', 'app');
                    $_data_array['max_width']  = $this->config->item('max_width', 'app');
                    $_data_array['max_height']  = $this->config->item('max_height', 'app');

                $this->_render_page('account', $_data_array);
            }
            else{
                $this->load->view('login.php');
            }
	}
        function update_password($Id)
	{
             if ($Id != '') {
              $this->form_validation->set_rules('password', $this->lang->line('create_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
              $this->form_validation->set_rules('password_confirm', $this->lang->line('create_user_validation_password_confirm_label'), 'required');

              if ($this->form_validation->run() == true)
              {
		$password = $this->input->post('password');
                $table="users";
		$nw_record_id = $this->ion_auth_model->update_password($table, $password, $Id);
                $this->session->set_flashdata('message', "Password change succesfully");
                redirect("user/account");
              }
              else{
                  $this->session->set_flashdata('message', "Password not change");
                   redirect("user/account");
              }
             }else{
                 redirect("user/account");
             }
		

	}
        public function update_account($Id)
	{
            if ($Id != '') {
                $this->load->helper(array('form', 'url'));
                   
                    $table = 'users';
                    $image=null;
                    $config['upload_path'] = './assets/user/image';
                    $config['allowed_types'] = 'gif|jpg|png';
                      $config['max_size']	= $this->config->item('max_size', 'app');
                    $config['max_width']  = $this->config->item('max_width', 'app');
                    $config['max_height']  = $this->config->item('max_height', 'app');

                    $this->load->library('upload', $config);
                   
                   
                            if ( ! $this->upload->do_upload('image'))
                            {
                                  
                                    if($_FILES["image"]["error"] == 4) {
                                      $image = $this->input->post('himage', true);
                                    }
                                    else{
                                        $error = array('error' => $this->upload->display_errors());
                                        $this->session->set_flashdata('message', "Error uploading image please check image size");
                                        redirect("user/account");
                                    }
                            }
                            else
                            {
                                    $data = array('upload_data' => $this->upload->data());
                                    $image = $data['upload_data']['file_name'];
                            }
                         
                     
                   
                   
                    
                    $user = $this->ion_auth->user()->row();
                   
               
                
		$nw_record_array = array(
                        'first_name' => $this->input->post('first_name'),
                        'last_name'  => $this->input->post('last_name'),
                        'image' => $image,
                        'zipcode' => $this->input->post('zipcode', true),
                        'address' => $this->input->post('address', true),
			'country' => $this->input->post('country', true),
                        'dob' => $this->input->post('dob', true),
                        'v_type' => $this->input->post('v_type', true),
                        'v_brand' => $this->input->post('v_brand', true),
                        'v_plate' => $this->input->post('v_plate', true)
			
		);
		$nw_record_id = $this->spot_model->update_account($table, $nw_record_array, $Id);

                redirect("user/account");
            } else {
                redirect("user/account");
            }
		
	}
        public function login()
	{
            if ($this->session->userdata('user_id')) {
                $this->data["user"] = $this->ion_auth->user()->row();
                redirect('user/account');
            }
            if($this->input->post()){

		//validate form input
		$this->form_validation->set_rules('user_email', 'User_email', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() == true)
		{
                   
			$remember = FALSE;

			if ($this->ion_auth->login($this->input->post('user_email'), $this->input->post('password'), $remember))
			{
				//redirect('user/account');

                                echo 'ok';
                                //exit();
			}
			else
			{
				// if the login was un-successful
				// redirect them back to the login page
                                //$this->data['message'] = "Incorrect email or password !";
                                //$this->_render_page('login', $this->data);

                                echo "incorrect email or password !";
                                //exit();
      }
		}
		else
		{
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
                        $this->_render_page('login', $this->data);
		}
            }
            else{
                 $this->load->view('login.php');
            }
	}
        function logout()
	{
		$logout = $this->ion_auth->logout();
                redirect('/');
	}
        public function registration()
	{
            
                if($this->input->post()){
                   
                    if($this->input->post('status')=="1"){
                        $active=1;
                    }
                    else{
                        $active=0;
                    }
                   
                     $tables = $this->config->item('tables','ion_auth');
                    // validate form input
                    $this->form_validation->set_rules('first_name', $this->lang->line('create_user_validation_fname_label'), 'required');
                    $this->form_validation->set_rules('last_name', $this->lang->line('create_user_validation_lname_label'), 'required');
                    $this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|valid_email|is_unique[' . $tables['users'] . '.email]');
                    $this->form_validation->set_rules('password', $this->lang->line('create_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
                    $this->form_validation->set_rules('password_confirm', $this->lang->line('create_user_validation_password_confirm_label'), 'required');

                    if ($this->form_validation->run() == true)
                    {
                        $email    = strtolower($this->input->post('email'));
                        $password = $this->input->post('password');
                        $additional_data = array(
                            'first_name' => $this->input->post('first_name'),
                            'last_name'  => $this->input->post('last_name'),
                            'username'  => "",
                            'phone'      => "",
                            'address'      =>"",
                            'city'      => "",
                            'gender'      => 0,
                        );
                    }
                    if ($this->form_validation->run() == true && $this->ion_auth->register($password, $email,$active, $additional_data))
                    {
                        $this->session->set_flashdata('message', $this->ion_auth->messages());
                        $this->data['message'] = $this->session->flashdata('message');
                        if($active){
                          //redirect('user/account');
                          echo "ok";
                        }else{
                             //$this->_render_page('login', $this->data);
                          //$this->load->view('login',$this->data);
                          echo $this->data['message'];
                        }
                       
                    }
                    else
                    {
                        $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
                        //$this->_render_page('registration', $this->data);
                        echo $this->data['message'];
                    }
                }
                else{

                  $this->load->view('registration.php');
                  //echo  validation_errors();
                }
	}
       //register with facebook or gmail
       public function registerwith($provider)
	{
          
		log_message('debug', "controllers.HAuth.login($provider) called");

		try
		{
			log_message('debug', 'controllers.HAuth.login: loading HybridAuthLib');
			$this->load->library('HybridAuthLib');

			if ($this->hybridauthlib->providerEnabled($provider))
			{
				log_message('debug', "controllers.HAuth.login: service $provider enabled, trying to authenticate.");
				$service = $this->hybridauthlib->authenticate($provider);

				if ($service->isUserConnected())
				{
					log_message('debug', 'controller.HAuth.login: user authenticated.');

					$user_profile = $service->getUserProfile();

					log_message('info', 'controllers.HAuth.login: user profile:'.PHP_EOL.print_r($user_profile, TRUE));
                                        $this->logouthybrid();
                                        $data['user_profile'] = $user_profile;
                                        $data['active'] = 1;
                                        $this->load->view('registration',$data);
				}
				else // Cannot authenticate user
				{
					show_error('Cannot authenticate user');
				}
			}
			else // This service is not enabled.
			{
				log_message('error', 'controllers.HAuth.login: This provider is not enabled ('.$provider.')');
				show_404($_SERVER['REQUEST_URI']);
			}
		}
		catch(Exception $e)
		{
			$error = 'Unexpected error';
			switch($e->getCode())
			{
				case 0 : $error = 'Unspecified error.'; break;
				case 1 : $error = 'Hybriauth configuration error.'; break;
				case 2 : $error = 'Provider not properly configured.'; break;
				case 3 : $error = 'Unknown or disabled provider.'; break;
				case 4 : $error = 'Missing provider application credentials.'; break;
				case 5 : log_message('debug', 'controllers.HAuth.login: Authentification failed. The user has canceled the authentication or the provider refused the connection.');
				         //redirect();
				         if (isset($service))
				         {
				         	log_message('debug', 'controllers.HAuth.login: logging out from service.');
				         	$service->logout();
				         }
				         show_error('User has cancelled the authentication or the provider refused the connection.');
				         break;
				case 6 : $error = 'User profile request failed. Most likely the user is not connected to the provider and he should to authenticate again.';
				         break;
				case 7 : $error = 'User not connected to the provider.';
				         break;
			}

			if (isset($service))
			{
				$service->logout();
			}

			log_message('error', 'controllers.HAuth.login: '.$error);
			show_error('Error authenticating user.');
		}
	}
         public function logouthybrid(){
            $this->load->library('HybridAuthLib');
            $this->hybridauthlib->logoutAllProviders();
        }
        public function endpoint()
	{

		log_message('debug', 'controllers.HAuth.endpoint called.');
		log_message('info', 'controllers.HAuth.endpoint: $_REQUEST: '.print_r($_REQUEST, TRUE));

		if ($_SERVER['REQUEST_METHOD'] === 'GET')
		{
			log_message('debug', 'controllers.HAuth.endpoint: the request method is GET, copying REQUEST array into GET array.');
			$_GET = $_REQUEST;
		}

		log_message('debug', 'controllers.HAuth.endpoint: loading the original HybridAuth endpoint script.');
		require_once APPPATH.'/third_party/hybridauth/index.php';
        }
        // activate the user
	function activate($id, $code=false)
	{
		if ($code !== false)
		{
			$activation = $this->ion_auth->activate($id, $code);
		}
		else if ($this->ion_auth->is_admin())
		{
			$activation = $this->ion_auth->activate($id);
		}

		if ($activation)
		{
			// redirect them to the auth page
			$this->session->set_flashdata('message', $this->ion_auth->messages());
			redirect("user/login", 'refresh');
		}
		else
		{
			// redirect them to the forgot password page
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			redirect("user/forgot_password", 'refresh');
		}
	}
        public function forgot_password()
	{
                 if (empty($_POST)){
              
                    $this->load->view('forgot_password.php');
                }
                else{
                    // setting validation rules by checking wheather identity is username or email
                    if($this->config->item('identity', 'ion_auth') != 'email' )
                    {
                       $this->form_validation->set_rules('identity', $this->lang->line('forgot_password_identity_label'), 'required');
                    }
                    else
                    {
                       $this->form_validation->set_rules('identity', $this->lang->line('forgot_password_validation_email_label'), 'required|valid_email');
                    }


                    if ($this->form_validation->run() == false)
                    {
                            $this->data['type'] = $this->config->item('identity','ion_auth');
                            // setup the input
                            $this->data['identity'] = array('name' => 'identity',
                                    'id' => 'identity',
                            );

                            // set any errors and display the form
                            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
                            $this->_render_page('user/forgot_password', $this->data);
                    }
                    else
                    {
                            $identity_column = $this->config->item('identity','ion_auth');
                            $identity = $this->ion_auth->where($identity_column, $this->input->post('identity'))->users()->row();

                            if(empty($identity)) {

                                    if($this->config->item('identity', 'ion_auth') != 'email')
                                    {
                                            $this->ion_auth->set_error('forgot_password_identity_not_found');
                                    }
                                    else
                                    {
                                       $this->ion_auth->set_error('forgot_password_email_not_found');
                                    }

                                    $this->session->set_flashdata('message', $this->ion_auth->errors());
                                    redirect("user/forgot_password", 'refresh');
                            }

                            // run the forgotten password method to email an activation code to the user
                            $forgotten = $this->ion_auth->forgotten_password($identity->{$this->config->item('identity', 'ion_auth')});

                            if ($forgotten)
                            {
                                    // if there were no errors
                                    $this->session->set_flashdata('message', $this->ion_auth->messages());
                                    redirect("user/login", 'refresh'); //we should display a confirmation page here instead of the login page
                            }
                            else
                            {
                                    $this->session->set_flashdata('message', $this->ion_auth->errors());
                                    redirect("user/forgot_password", 'refresh');
                            }
                    }
                
                }
	}
        public function reset_password($code = NULL)
	{
		if (!$code)
		{
			show_404();
		}

		$user = $this->ion_auth->forgotten_password_check($code);
               

		if ($user)
		{
			// if the code is valid then display the password reset form
                        
			$this->form_validation->set_rules('new', $this->lang->line('reset_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
			$this->form_validation->set_rules('new_confirm', $this->lang->line('reset_password_validation_new_password_confirm_label'), 'required');

			if ($this->form_validation->run() == false)
			{
				// display the form
                            
				// set the flash data error message if there is one
				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

				
				$this->data['user_id'] = $user->id;
				$this->data['code'] = $code;
                               // render
				 $this->load->view('reset_password.php', $this->data);
                               
			}
			else
			{
				// do we have a valid request?
				
				
					// finally change the password
					$identity = $user->{$this->config->item('identity', 'ion_auth')};

					$change = $this->ion_auth->reset_password($identity, $this->input->post('new'));

					if ($change)
					{
						// if the password was successfully changed
						$this->session->set_flashdata('message', $this->ion_auth->messages());
						redirect("user/login", 'refresh');
					}
					else
					{
						$this->session->set_flashdata('message', $this->ion_auth->errors());
						redirect('user/reset_password/' . $code, 'refresh');
					}
				
			}
		}
		else
		{
			// if the code is invalid then send them back to the forgot password page
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			redirect("user/forgot_password", 'refresh');
		}
                
	}
        function _get_csrf_nonce()
	{
		$this->load->helper('string');
		$key   = random_string('alnum', 8);
		$value = random_string('alnum', 20);
		$this->session->set_flashdata('csrfkey', $key);
		$this->session->set_flashdata('csrfvalue', $value);

		return array($key => $value);
	}
       

        function _render_page($view, $data=null, $returnhtml=false)//I think this makes more sense
	{

		$this->viewdata = (empty($data)) ? $this->data: $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);

		if ($returnhtml) return $view_html;//This will return html on 3rd argument being true
	}



}
