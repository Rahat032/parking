<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library(array('ion_auth', 'form_validation'));
        $this->load->model('ion_auth_model');
        $this->load->model('spot_model');
        $this->load->library(array('session'));
    }

    public function user_detail($uId) {
        //echo $uId;
        if ($uId != '') {
            $user = $this->spot_model->get_user('users', $uId);
            $spots = $this->spot_model->get_spotlist('spot', $uId);
            $data['user'] = $user;
            $data['spots'] = $spots;
            $this->load->view('user_detail_view', $data);
        } else {
            redirect("spot/findspot");
        }
    }

    public function account_close() {

        if ($this->session->userdata('user_id')) {

            $table = 'spot';
            $userId = $this->session->userdata('user_id');
            $spots = $this->spot_model->get_spotlist($table, $userId);
            $can_delete = TRUE;
            foreach ($spots as $spot) {

                if ($spot['active'] == 0) {
                    $can_delete = FALSE;
                    break;
                }
            }

            $_data_array['can_delete'] = $can_delete;
            $_data_array['active'] = 3;
            $this->load->view('account_close', $_data_array);
        } else {
            $this->load->view('index.php');
        }
    }

    public function inactivate_user() {

        if ($this->session->userdata('user_id')) {
//            $user = $this->ion_auth->user()->row();
//            print_r($user);
            $id = $this->session->userdata('user_id');

            $nw_record_array = array(
                'active' => 0
            );
            $nw_record_id = $this->spot_model->update_account('users', $nw_record_array, $id);

//            $user = $this->ion_auth->user()->row();
//            print_r($user);
            $logout = $this->ion_auth->logout();
            redirect('/');
        } else {
            $this->load->view('index.php');
        }


//             $user = $this->ion_auth->user()->row();
//                   
//               
//                
//		$nw_record_array = ;
//		$nw_record_id = $this->spot_model->update_account($table, $nw_record_array, $Id);
//
//                redirect("user/account");
    }

}
