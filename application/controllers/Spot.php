<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Spot extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
         * 
	 */
        function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model('spot_model');
		
		$this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url','language'));
                $this->load->config('app', TRUE);
		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
        }
        public function addspot()
	{
                if (empty($_POST)){
                  redirect("spot/list_spot");  
                }
                    $table = 'spot';
                
                    $config['upload_path'] = './assets/image';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size']	= '1000';
                    $config['max_width']  = '1524';
                    $config['max_height']  = '768';

                    $this->load->library('upload', $config);

                    if ( ! $this->upload->do_upload('image'))
                    {
                            $error = array('error' => $this->upload->display_errors());
                    }
                    else
                    {
                            $data = array('upload_data' => $this->upload->data());
                    }
                    
                    $image = $data['upload_data']['file_name'];
                    $user = $this->ion_auth->user()->row();
                    $userId= $user->id;
               
                
		$nw_record_array = array(
            'user_id' => $userId,
			'street' => $this->input->post('street', true),
			'zip' => $this->input->post('zip', true),
			'city' => $this->input->post('city', true),
                        'state' => $this->input->post('state', true),
			'country' => $this->input->post('country', true),
			'image' => $image,
			'spot_type' => $this->input->post('spot_type', true),
			'spot_available_service' => $this->input->post('spot_available_service', true),
			'price' => $this->input->post('price', true),
			'dateFrom'=>  $this->input->post('from', true),
			'dateTo'=> $this->input->post('to', true),
                    	'active' => 1,
                        'latitude'=>  $this->input->post('latitude', true),
			'longitude'=> $this->input->post('longitude', true)
			
		);
		
		$nw_record_id = $this->spot_model->add_nw_record($table, $nw_record_array);
                redirect("spot/list_spot");
	}
        public function findrelaventSpot(){
           
            $this->load->library("session");
            if($this->input->post('latitude', true)!=null && $this->input->post('longitude', true) !=null )
            {
                $this->session->set_userdata("lat", $this->input->post('latitude', true));
                $this->session->set_userdata("lang", $this->input->post('longitude', true));
                
            }
            echo "location";
            //if($this->session->userdata("lang")!=null){die($this->session->userdata("lang"));}
        }
        public function findspot(){
           
            $this->load->library("session");
            if($this->session->userdata("lat")!=null && $this->session->userdata("lang")!=null){
               $table = 'spot';
               $lat=$this->session->userdata("lat");$lang=$this->session->userdata("lang");
               
               //$user = $this->ion_auth->user()->row();
               //$userId= $user->id;
               $record_list = $this->spot_model->get_releventspotlist($table,$lat,$lang);
                $data['record_list']=$record_list;
              // $_data_array['active'] = 3;
               $this->load->library('googlemaps');
                if($this->session->userdata("lat")!=null && $this->session->userdata("lang")!=null){
                 $lat=$this->session->userdata("lat");$lang=$this->session->userdata("lang");
                 $config['center'] = $lat.", ".$lang;
                }
  
                $config['zoom'] = $this->config->item('map_zoom', 'app');
                $this->googlemaps->initialize($config);
                 if($this->session->userdata("lat")!=null && $this->session->userdata("lang")!=null){
                        $lat=$this->session->userdata("lat");$lang=$this->session->userdata("lang");
            
          
                $marker = array();
                $position= $lat.", ".$lang ;
               // die($position);
                $marker['position'] =$position;
                //$address=$map['street']." ".$map['city'].", ".$map['state'].", ".$map['country'];
                //$marker['infowindow_content'] = $address;
                $marker['icon'] = 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=A|9999FF|000000';
                $this->googlemaps->add_marker($marker);
             }
            foreach($record_list as $map){ 
                $lat= $map['latitude'];
                $lang= $map['longitude'];
                
                $marker = array();
                $position= $lat.", ".$lang ;
               // die($position);
                $marker['position'] =$position;
                 $address=$map['address'];
                $marker['infowindow_content'] = $address;
                if($map['active'] != "2"){
                     $marker['icon'] = $this->config->item('base_url', 'app')."/assets/img/available.png";
                } else{
                    $marker['icon'] = $this->config->item('base_url', 'app')."/assets/img/notavailable.png";
                }
                //$marker['icon'] = 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=A|9999FF|000000';
                $this->googlemaps->add_marker($marker);
            }
            $data['map'] = $this->googlemaps->create_map();
              $data['zoom'] = $config['zoom'];
               $this->load->view("relevantspot", $data);
               
                
            }
        }
        public function advancesearch(){
           $table = 'spot';
           
           //session data 
           $zoom=null;
           $this->load->library("session");
            if($this->input->post('from', true)!=null && $this->input->post('to', true) !=null && $this->input->post('address', true) !=null )
            {
                $this->session->set_userdata("from", $this->input->post('from', true));
                $this->session->set_userdata("to", $this->input->post('to', true));
                $this->session->set_userdata("address", $this->input->post('address', true));
                
            }
            if($this->input->post('address', true) !=null )
            {
                $this->session->set_userdata("address", $this->input->post('address', true));
                $this->session->set_userdata("lata", $this->input->post('lata', true));
                $this->session->set_userdata("lngb", $this->input->post('lngb', true));
                $this->session->set_userdata("latc", $this->input->post('latc', true));
                $this->session->set_userdata("lngd", $this->input->post('lngd', true));
                
            }
            if($this->input->post('zoom', true) !=null )
            {
                
                $this->session->set_userdata("zoom", $this->input->post('zoom', true));
                $zoom=$this->input->post('zoom', true);
            }
                $this->session->set_userdata("latitudeA", $this->input->post('latitudeA', true));
                $this->session->set_userdata("longitudeA", $this->input->post('longitudeA', true));
          
            $latitudeA=$this->input->post('latitudeA', true);
            $longitudeA=$this->input->post('longitudeA', true);
            $lata=$this->input->post('lata', true);
            $langb=$this->input->post('lngb', true);
            $latc=$this->input->post('latc', true);
            $langd=$this->input->post('lngd', true);
            $address=$this->input->post('address', true);
            $spot_type=$this->input->post('spot_type', true);
            $from=$this->input->post('from', true);
            $to=$this->input->post('to', true);
            $record_list = $this->spot_model->advanceSearch($table,$address,$latitudeA,$longitudeA,$lata,$langb,$latc,$langd,$spot_type,$from,$to);
           
           
          
           $data['record_list']=$record_list;
           $this->load->library('googlemaps');
          
            $config['center'] = $latitudeA.", ".$longitudeA;
            $config['zoom'] = $this->config->item('map_zoom', 'app');
            
             
            $this->googlemaps->initialize($config);
            
          
                $marker = array();
                $position= $latitudeA.", ".$longitudeA;
               // die($position);
                $marker['position'] =$position;
                //$address=$map['street']." ".$map['city'].", ".$map['state'].", ".$map['country'];
                //$marker['infowindow_content'] = $address;
              //  $marker['icon'] = 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=A|9999FF|000000';
                $this->googlemaps->add_marker($marker);
             
            foreach($record_list as $map){ 
                $lat= $map['latitude'];
                $lang= $map['longitude'];
                
                $marker = array();
                $position= $lat.", ".$lang ;
               // die($position);
                $marker['position'] =$position;
                $address=$map['address'];
                $marker['infowindow_content'] = $address;
                if($map['active'] != "2"){
                     $marker['icon'] = $this->config->item('base_url', 'app')."/assets/img/available.png";
                } else{
                    $marker['icon'] = $this->config->item('base_url', 'app')."/assets/img/notavailable.png";
                }
               
                 
                $this->googlemaps->add_marker($marker);
            }
            $data['map'] = $this->googlemaps->create_map();
            
            
            
            $this->load->view("relevantspot", $data);
        }
        //ajax call for getting resulut
        
        public function getspotresult(){
           $table = 'spot';
           
           //session data 
           $zoom=null;
           $this->load->library("session");
            if($this->input->post('from', true)!=null && $this->input->post('to', true) !=null && $this->input->post('address', true) !=null )
            {
              
                $this->session->set_userdata("from", $this->input->post('from', true));
                $this->session->set_userdata("to", $this->input->post('to', true));
                $this->session->set_userdata("address", $this->input->post('address', true));
               
                
            }
            if($this->input->post('address', true) !=null )
            {
                $this->session->set_userdata("address", $this->input->post('address', true));
                $this->session->set_userdata("lata", $this->input->post('lata', true));
                $this->session->set_userdata("lngb", $this->input->post('lngb', true));
                $this->session->set_userdata("latc", $this->input->post('latc', true));
                $this->session->set_userdata("lngd", $this->input->post('lngd', true));
                
            }
            
            $zoom=$this->input->post('zoom', true);
           
            $this->session->set_userdata("latitudeA", $this->input->post('latitudeA', true));
            $this->session->set_userdata("longitudeA", $this->input->post('longitudeA', true));
          
            $latitudeA=$this->input->post('latitudeA', true);
            $longitudeA=$this->input->post('longitudeA', true);
            $lata=$this->input->post('lata', true);
            $langb=$this->input->post('lngb', true);
            $latc=$this->input->post('latc', true);
            $langd=$this->input->post('lngd', true);
            $address=$this->input->post('address', true);
            $spot_type=$this->input->post('spot_type', true);
            $from=$this->input->post('from', true);
            $to=$this->input->post('to', true);
            //var_dump($latc);exit;
            $record_list = $this->spot_model->advanceSearch($table,$address,$latitudeA,$longitudeA,$lata,$langb,$latc,$langd,$spot_type,$from,$to);
           
           
          
           $data['record_list']=$record_list;
           $this->load->library('googlemaps');
          
            $config['center'] = $latitudeA.", ".$longitudeA;
            if($zoom != null){
              
                $config['zoom'] = $zoom;
            }else{
               $config['zoom'] = $this->config->item('map_zoom', 'app');
            }
             
            $this->googlemaps->initialize($config);
            
          
                $marker = array();
                $position= $latitudeA.", ".$longitudeA;
               // die($position);
                $marker['position'] =$position;
                //$address=$map['street']." ".$map['city'].", ".$map['state'].", ".$map['country'];
                //$marker['infowindow_content'] = $address;
              //  $marker['icon'] = 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=A|9999FF|000000';
                $this->googlemaps->add_marker($marker);
             
            foreach($record_list as $map){ 
                $lat= $map['latitude'];
                $lang= $map['longitude'];
                
                $marker = array();
                $position= $lat.", ".$lang ;
               // die($position);
                $marker['position'] =$position;
                $address=$map['address'];
                $marker['infowindow_content'] = $address;
                if($map['active'] != "2"){
                     $marker['icon'] = $this->config->item('base_url', 'app')."/assets/img/available.png";
                } else{
                    $marker['icon'] = $this->config->item('base_url', 'app')."/assets/img/notavailable.png";
                }
               
                 
                $this->googlemaps->add_marker($marker);
            }
            $data['map'] = $this->googlemaps->create_map();
            $json = json_encode($data); // Note JSON_FORCE_OBJECT removed
            echo $json;
           // print_r($data);
            
            
           // $this->load->view("relevantspot", $data);
        }
        public function detail($Id) {
		if ($Id != '') {
                     $table = 'spot';
		    $_data_array['record_list'] = $this->spot_model->get_spot_details($table,$Id);
        //echo $_data_array['record_list'][0]['user_id'];
        $uId = $_data_array['record_list'][0]['user_id'];
        $owner = $this->spot_model->get_user('users',$uId);
        $_data_array['spot_owner']= $owner[0]['first_name']." ".$owner[0]['last_name'];
		    $this->load->view('spotdetail', $_data_array);
		} else {
                    redirect("spot/findspot");
		}
	}
}
