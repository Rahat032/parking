<?php

class Migrate extends CI_Controller
{

        public function index()
        {
                $this->load->library('migration');
                //$this->migration->version(3);

                if ($this->migration->latest() == FALSE)
                {
                        show_error($this->migration->error_string());
                }
                echo $this->migration->latest();
        }

}
?>