<?php
class Seed extends CI_Controller
{

        public function seed_user()
        {
                $data = array(
			'id' => '1',
			'ip_address' => '127.0.0.1',
			'password' => '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36',
			'salt' => '',
			'email' => 'admin@admin.com',
			'activation_code' => '',
			'forgotten_password_code' => NULL,
			'created_on' => '1268889823',
			'last_login' => '1268889823',
			'active' => '1',
			'first_name' => 'Admin',
			'last_name' => 'istrator',
			'phone' => '0',
		);
		$this->db->insert('users', $data);
                
               
                echo "insert";
        }
         public function seed_spot()
        {
             // default spot Type Valuse
               $data1 = array(
			'id' => '1',
			'spot_type' => 'Driveway'
		);
		$this->db->insert('spot_type', $data1);
                $data2 = array(
			'id' => '2',
			'spot_type' => 'Garage'
		);
		$this->db->insert('spot_type', $data2);
                $data3 = array(
			'id' => '3',
			'spot_type' => 'Laneway'
		);
		$this->db->insert('spot_type', $data3);
                $data4 = array(
			'id' => '4',
			'spot_type' => 'Parking Lot'
		);
		$this->db->insert('spot_type', $data4);
                
                // default spot available services valus
                 $data5 = array(
			'id' => '1',
			'spot_available_services' => 'Authorized type of vehicle'
		);
		$this->db->insert('spot_available_services', $data5);
                $data6 = array(
			'id' => '2',
			'spot_available_services' => 'Invalid person'
		);
		$this->db->insert('spot_available_services', $data6);
                $data7 = array(
			'id' => '3',
			'spot_available_services' => 'Public transport'
		);
		$this->db->insert('spot_available_services', $data7);
                
                // default price values 
                
                 // default spot available services valus
                 $data8 = array(
			'id' => '1',
			'period' => 'Hourly',
                        'min' => '1',
                        'max' => '5'
		);
		$this->db->insert('spot_price', $data8);
                $data9 = array(
			'id' => '2',
			'period' => 'Daily',
                        'min' => '8',
                        'max' => '20'
		);
		$this->db->insert('spot_price', $data9);
                $data10 = array(
			'id' => '3',
			'period' => 'Weekly',
                        'min' => '60',
                        'max' => '150'
		);
		$this->db->insert('spot_price', $data10);
                $data11 = array(
			'id' => '4',
			'period' => 'Monthly',
                        'min' => '200',
                        'max' => '600'
		);
		$this->db->insert('spot_price', $data11);
                
                
                echo "insert";
        }
        public function mail (){
             $this->load->library('email');
             $result = $this->email
            ->from('support@parkingslot.com')
            ->reply_to('noreplay@parkingslot.com')    // Optional, an account where a human being reads.
            ->to("chutischoolsa@gmail.com")
            ->subject("test")
            ->message("Hello")
            ->send();
                echo $this->email->print_debugger();
            var_dump($result);exit();
        }
        public function query(){
            $data = array(
               'version' => 1
            );
            $this->db->update('migrations', $data); 
            echo "updated";
        }
        
}
?>