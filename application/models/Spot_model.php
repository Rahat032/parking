<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Datacap Model class
*
* This class performs all the interactions with the database
*
* @version 1.0
* @author Srijidh Madhavan
* @project Datacapture
*/

class Spot_model Extends CI_Model {

	
	public function add_nw_user($nw_user_array) {
		$this->db->insert('data_users', $nw_user_array);
		return $this->db->insert_id();
	}

	public function edit_user($modified_user_array, $userId) {
		$this->db->where('userId', $userId);
		$this->db->update('data_users', $modified_user_array);
		return TRUE;
	}

	public function delete_user($userId) {
		$this->db->where('userId', $userId);
		$this->db->delete('data_users');
		return TRUE;
	}

	
	function add_nw_record($table, $nw_record_array) {
		$this->db->insert($table, $nw_record_array);
		return $this->db->insert_id();
	}
	
	function update_record($table, $record_array, $recordId) {
		
		if ( $table == 'data_records') {
			$this->db->where('record_Id', $recordId);
		} else {
			$this->db->where('record_key_id', $recordId);
		}
		
		$this->db->set($record_array);
		$this->db->update($table);
		return $recordId;
		
	}

	function get_spot_details($table, $Id) {
		$this->db->select()->from($table)
                        ->where('id', $Id);
                $query = $this->db->get();
		return $query->result_array();
	}
        public function get_spotlist($table,$userId) {
		
		$this->db->select()->from($table)
                ->where('user_id', $userId);
                $query = $this->db->get();
		return $query->result_array();
	}
        function update_spot($table, $nw_record_array, $Id) {
		
		$this->db->where('id', $Id);
		$this->db->set($nw_record_array);
		$this->db->update($table);
		return $Id;
		
	}
        public function update_status($Id,$data) {
		$this->db->where('id', $Id);
		$this->db->update('spot',$data);
		return TRUE;
	}
        public function delete_spot($Id) {
		$this->db->where('id', $Id);
                $this->db->where('active !=', 0);
		$this->db->delete('spot');
		return TRUE;
	}
        public function get_releventspotlist($table,$lat,$lang) {
		
            
                
		$query = $this->db->query('SELECT * , ( 6371 * acos( cos( radians('.$lat.') ) * cos( radians( latitude ) ) * 
                cos( radians( longitude ) - radians('.$lang.') ) + sin( radians('.$lat.') ) * 
                sin( radians( latitude ) ) ) ) AS distance FROM spot where active != 0 HAVING
                distance < 3.2 ORDER BY distance asc');
                //var_dump($query);exit();
		return $query->result_array();
	}
        
         public function advanceSearch($table,$address,$latitudeA,$longitudeA,$lata,$langb,$latc,$langd,$spot_type,$from,$to) {
		
		
                $qur = 'SELECT * , ( 6371 * acos( cos( radians('.$latitudeA.') ) * cos( radians( latitude ) ) * 
                cos( radians( longitude ) - radians('.$longitudeA.') ) + sin( radians('.$latitudeA.') ) * 
                sin( radians( latitude ) ) ) ) AS distance FROM spot where active != 0';
               
                $qur .= " and latitude < ".$lata." and latitude > ".$latc. " and longitude < " .$langb. " and longitude > " .$langd;
                //var_dump($qur);exit;
                if($spot_type!=""){
                       $qur .= " and spot_type = '".trim($spot_type)."' ";
                }
                if($from!="" && $to!=""){
                    $qur .= " and dateFrom BETWEEN '".trim($from)."' and '" .trim($to)."'";
                     $qur .= " and dateTo BETWEEN '".trim($from)."' and '" .trim($to)."'";
                }
                $qur .= " order by distance";
                $query = $this->db->query($qur);
                return $query->result_array();
             
		
	}
        function update_account($table, $nw_record_array, $Id) {
		
		$this->db->where('id', $Id);
		$this->db->set($nw_record_array);
		$this->db->update($table);
		return $Id;
		
	}
        
        public function get_user($table,$userId) {
		
		$this->db->select()->from($table)
                ->where('id', $userId);
                $query = $this->db->get();
		return $query->result_array();
	}
	public function get_price() {
		
		$this->db->select()->from('spot_price');
                $query = $this->db->get();
               
		return $query->result_array();
	}
        
        public function get_type() {
		
		$this->db->select()->from('spot_type');
                $query = $this->db->get();
		return $query->result_array();
	}
        
        public function get_available_services() {
		
		$this->db->select()->from('spot_available_services');
                $query = $this->db->get();
		return $query->result_array();
	}
	
}