<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


// image size
$config['max_size']= '1000';
$config['max_width']  = '1524';
$config['max_height']  = '768';
// map_zoom
$config['map_zoom']= '13';

//base url
$config['base_url'] = 'http://'.$_SERVER['SERVER_NAME'].'/parking';


