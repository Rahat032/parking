
  <div class="page-info" style="background: url(<?php echo base_url(); ?>images/bg.jpg); background-size:cover">
    <div class="container text-center section-promo"> 
    	<div class="row">
        	<div class="col-sm-3 col-xs-6 col-xxs-12">
                <div class="iconbox-wrap">
                          <div class="iconbox">
                            <div class="iconbox-wrap-icon">
                            <i class="icon  icon-group"></i>
                            </div>
                            <div class="iconbox-wrap-content">
                              <h5><span>2200</span> </h5>
                              <div  class="iconbox-wrap-text">Trusted Seller</div>
                            </div>
                          </div>
  							<!-- /..iconbox -->
                     </div><!--/.iconbox-wrap-->
            </div>
            
            <div class="col-sm-3 col-xs-6 col-xxs-12">
            	<div class="iconbox-wrap">
                          <div class="iconbox">
                            <div class="iconbox-wrap-icon">
                            <i class="icon  icon-th-large-1"></i>
                            </div>
                            <div class="iconbox-wrap-content">
                              <h5><span>100</span> </h5>
                              <div  class="iconbox-wrap-text">Categories</div>
                            </div>
                          </div>
  							<!-- /..iconbox -->
                     </div><!--/.iconbox-wrap-->
            </div>
            
            <div class="col-sm-3 col-xs-6  col-xxs-12">
            	<div class="iconbox-wrap">
                          <div class="iconbox">
                            <div class="iconbox-wrap-icon">
                            <i class="icon  icon-map"></i>
                            </div>
                            <div class="iconbox-wrap-content">
                              <h5><span>700</span> </h5>
                              <div  class="iconbox-wrap-text">Location</div>
                            </div>
                          </div>
  							<!-- /..iconbox -->
                     </div><!--/.iconbox-wrap-->
            </div>
            
            <div class="col-sm-3 col-xs-6 col-xxs-12">
            	<div class="iconbox-wrap">
                          <div class="iconbox">
                            <div class="iconbox-wrap-icon">
                            <i class="icon icon-facebook"></i>
                            </div>
                            <div class="iconbox-wrap-content">
                              <h5><span>50,000</span> </h5>
                              <div  class="iconbox-wrap-text"> Facebook Fans</div>
                            </div>
                          </div>
  							<!-- /..iconbox -->
                     </div><!--/.iconbox-wrap-->
            </div>
            
        </div>
    
    </div>
  </div>
  <!-- /.page-info -->
  
  <div class="page-bottom-info">
      <div class="page-bottom-info-inner">
      
      	<div class="page-bottom-info-content text-center">
        	<h1>If you have any questions, comments or concerns, please call the Classified Advertising department at (000) 555-5555</h1>
            <a class="btn  btn-lg btn-primary-dark" href="tel:+000000000">
            <i class="icon-mobile"></i> <span class="hide-xs color50">Call Now:</span> (000) 555-5555   </a>
        </div>
      
      </div>
  </div>

  
  <div class="footer" id="footer">
    <div class="container">
      <ul class=" pull-left navbar-link footer-nav">
        <li><a href="<?php echo base_url(); ?>home/index/"> Home </a>
            <a href="about-us.html"> About us </a>
            <a href="<?php echo base_url(); ?>home/terms/"> Terms and Conditions </a> 
            <a href="<?php echo base_url(); ?>home/privacy/"> Privacy Policy </a> 
            <a href="contact.html"> Contact us </a> 
            <a href="<?php echo base_url(); ?>home/faq/"> FAQ </a>
      </ul>
      <ul class=" pull-right navbar-link footer-nav">
        <li> &copy; 2015 REZAPARK </li>
      </ul>
    </div>
    
  </div>
  <!-- /.footer --> 
</div>
<!-- /.wrapper --> 

<!-- Le javascript
================================================== --> 

<!-- Placed at the end of the document so the pages load faster --> 
 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"> </script>
<script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script> 
<script src="<?php echo base_url(); ?>assets/js/include/jquery.datetimepicker.js"></script> 
<!-- include carousel slider plugin  --> 
<script src="<?php echo base_url(); ?>assets/js/owl.carousel.min.js"></script> 

<!-- include equal height plugin  --> 
<script src="<?php echo base_url(); ?>assets/js/jquery.matchHeight-min.js"></script> 

<!-- include jquery list shorting plugin plugin  --> 
<script src="<?php echo base_url(); ?>assets/js/hideMaxListItem.js"></script> 

<!-- include jquery.fs plugin for custom scroller and selecter  --> 
<script src="<?php echo base_url(); ?>assets/plugins/jquery.fs.scroller/jquery.fs.scroller.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery.fs.selecter/jquery.fs.selecter.js"></script>


<!-- include custom script for site  --> 
<script src="<?php echo base_url(); ?>assets/js/script.js"></script>




<!-- include jquery autocomplete plugin  -->
<!--
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/autocomplete/jquery.mockjax.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/autocomplete/jquery.autocomplete.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/autocomplete/usastates.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/autocomplete/autocomplete-demo.js"></script>
-->


</body>
</html>
