<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(); ?>assets/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>assets/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>assets/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>ico/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/ico/favicon.png">
<title>REZAPARK</title>
<!-- Bootstrap core CSS -->
<link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">

<!-- styles needed for carousel slider -->
<link href="<?php echo base_url(); ?>assets/css/include/jquery.datetimepicker.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/owl.carousel.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/owl.theme.css" rel="stylesheet">
<!--UISLIDER include Css Files -->

<link href="<?php echo base_url(); ?>assets/css/include/nouislider.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/include/nouislider.min.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/include/nouislider.pips.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/include/nouislider.tooltips.css" rel="stylesheet">

<!-- Just for debugging purposes. -->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

<!-- include pace script for automatic web page progress bar  -->

<script>
    paceOptions = {
      elements: true
    };
</script>
<script src="<?php echo base_url(); ?>assets/js/pace.min.js"></script>
</head>
<body>
<div id="wrapper">
  <div class="header">
    <nav class="navbar navbar-site navbar-default" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
          <a href="<?php echo base_url();?>user/account/" class="navbar-brand logo logo-title"> 
          <!-- Original Logo will be placed here  --> 
          <span class="logo-icon"><i><img style=" height: 30px; width: 30px; margin-right: -9px; " src="<?php echo base_url(); ?>assets/ico/apple-touch-icon-144-precomposed.png"></i> </span> REZA<span>PARK </span> </a> </div>
        <div class="navbar-collapse collapse">
          
          <ul class="nav navbar-nav navbar-right">
            <li><a href="<?php echo base_url();?>user/logout/">Signout <i class="glyphicon glyphicon-off"></i> </a></li>
            <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span><?= $this->session->userdata('fname'); ?> <?= $this->session->userdata('lname'); ?></span> <i class="icon-user fa"></i> <i class=" icon-down-open-big fa"></i></a>
              <ul class="dropdown-menu user-menu">
                <li class="active"><a href="<?php echo base_url();?>user/account/"><i class="icon-home"></i>Edit Account</a></li>
             </ul>
            </li>
            <li class="postadd"><a class="btn btn-block   btn-border btn-post btn-danger" onclick="getLocation()">Find a Spot</a></li>
          </ul>
        </div>
        <!--/.nav-collapse --> 
      </div>
      <!-- /.container-fluid --> 
    </nav>
  </div>
  <!-- /.header -->
  <script>

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else { 
        alert("Geolocation is not supported by this browser.");
    }
}

function showPosition(position) {
//    alert("Latitude: " + position.coords.latitude + 
//    "<br>Longitude: " + position.coords.longitude);
    
    var dataString = 'latitude='+ position.coords.latitude + '&longitude='+ position.coords.longitude;
    // AJAX Code To Submit Form.
    $.ajax({
            type: "POST",
            url: "<?=base_url();?>spot/findrelaventSpot",
            data: dataString,
            success: function(result){
                 window.location.href = "<?=base_url();?>spot/findspot/";
            }
    });
     
    
}
</script>