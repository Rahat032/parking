<div class="col-sm-3 page-sidebar">
          <aside>
            <div class="inner-box">
              <div class="user-panel-sidebar">
                <div class="collapse-box">
                  <h5 class="collapse-title no-border"> My Classified <a href="#MyClassified" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>
                  <div class="panel-collapse collapse in" id="MyClassified">
                    <ul class="acc-list">
                      <li <?php if(isset($active) && $active==1) echo "class='active'" ?>><a  href="<?=base_url();?>user/account"><i class="icon-home"></i> Personal Home </a></li>
                      
                    </ul>
                  </div>
                </div>
                <!-- /.collapse-box  -->
                <div class="collapse-box">
                  <h5 class="collapse-title"> Spot <a href="#MyAds" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>
                  <div class="panel-collapse collapse in" id="MyAds">
                    <ul class="acc-list">
                      <li <?php if(isset($active) &&$active==2) echo "class='active'" ?>><a href="<?=base_url();?>user/addspot"><i class="icon-docs"></i> Add Spot</a></li>
                      <li><a <?php if(isset($active) &&$active==3) echo "class='active'" ?> href="<?=base_url();?>user/list_spot"><i class="icon-star-circled"></i> List of My Spot</a></li>
                    </ul>
                  </div>
                </div>
                <!-- /.collapse-box  -->
                
              

                <div class="collapse-box">
                  <h5 class="collapse-title"> Terminate Account <a href="#TerminateAccount" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>
                  <div class="panel-collapse collapse in" id="TerminateAccount">
                    <ul class="acc-list">
                      <li><a <?php if(isset($active) &&$active==4) echo "class='active'" ?> href="<?=base_url();?>users/account_close"><i class="icon-star-circled"></i> Close account </a></li>
                    </ul>
                  </div>
                </div>
                <!-- /.collapse-box  --> 
              </div>
            </div>
            <!-- /.inner-box  --> 
            
          </aside>
        </div>
        <!--/.page-sidebar-->