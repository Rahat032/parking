   <div class="footer" id="footer">
    <div class="container">
      <ul class=" pull-left navbar-link footer-nav">
        <li><a href="<?php echo base_url(); ?>user/account/"> Home </a> 
            <a href="about-us.html"> About us </a> 
            <a href="<?php echo base_url(); ?>/home/terms"> Terms and Conditions </a> 
            <a href="<?php echo base_url(); ?>/home/privacy"> Privacy Policy </a> 
            <a href="contact.html"> Contact us </a>
            <a href="<?php echo base_url(); ?>home/faq/"> FAQ </a>
      </ul>
      <ul class=" pull-right navbar-link footer-nav">
        <li> &copy; 2015 RezaPark </li>
      </ul>
    </div>
  </div>
  <!--/.footer--> 
</div>
<!-- /.wrapper --> <!-- Le javascript
================================================== --> 

<!-- Placed at the end of the document so the pages load faster --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"> </script><script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script> 


<!-- include carousel slider plugin  --> 
<script src="<?php echo base_url(); ?>assets/js/owl.carousel.min.js"></script>

<!-- include interactive alert plugin  --> 
<script src="<?php echo base_url(); ?>assets/js/bootbox.min.js"></script>

<!-- include equal height plugin  -->
<script src="<?php echo base_url(); ?>assets/js/jquery.matchHeight-min.js"></script>

<!-- include jquery list shorting plugin plugin  -->
<script src="<?php echo base_url(); ?>assets/js/hideMaxListItem.js"></script>

<!-- include jquery.fs plugin for custom scroller and selecter  --> 
<script src="<?php echo base_url(); ?>assets/plugins/jquery.fs.scroller/jquery.fs.scroller.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery.fs.selecter/jquery.fs.selecter.js"></script>
<script src="<?php echo base_url(); ?>assets/js/include/jquery.datetimepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/include/main.js"></script>
<script src="<?php echo base_url(); ?>assets/js/include/nouislider.js"></script>
<script src="<?php echo base_url(); ?>assets/js/include/nouislider.min.js"></script>
<!-- include custom script for site  --> 
<script src="<?php echo base_url(); ?>assets/js/script.js"></script>
</body>
</html>
