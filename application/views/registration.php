<?php include ("partial/header.php"); ?> 



<div class="main-container">
    <div class="container">
      <div class="row">
        <div class="col-md-12 page-content">
          <div class="inner-box category-content">
            <h2 class="title-2"> <i class="icon-user-add"></i> Create your account</h2>
            <div class="row">
              <div class="col-sm-12">
                <form class="form-horizontal" id="my_form" method="post" accept-charset="utf-8">
                  <fieldset>
                    <div class="form-group">
                      <!-- <div id="error">
                          error will be shown here !
                      </div> -->
                      <?php //if(isset($message)){ ?>
                        <div class="col-md-3"></div>
                            <div class="col-md-6 alert alert-danger" id="error" style="padding: 3px; color: red;font-family: sans-serif; display: none;"></div>
                      <?php// } ?>
                    </div>
                    <!-- Text input-->
                    <div class="form-group">
                      <label class="col-md-3 control-label" >First Name <sup>*</sup></label>
                      <div class="col-md-4">
                        <input  name ="first_name" id ="first_name" placeholder="First Name" class="form-control input-md"  type="text" required value="<?php if (isset($user_profile->firstName)) echo $user_profile->firstName;?>">
                      </div>
                    </div>
                    
                    <!-- Text input-->
                    <div class="form-group">
                      <label class="col-md-3 control-label" >Last Name <sup>*</sup></label>
                      <div class="col-md-4">
                        <input  name ="last_name" id ="last_name" placeholder="Last Name" class="form-control input-md" type="text" required value="<?php if (isset($user_profile->lastName)) echo $user_profile->lastName;?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputEmail3" class="col-md-3 control-label">Email <sup>*</sup></label>
                      <div class="col-md-4">
                        <input type="email" class="form-control" name ="email" id ="email" placeholder="Email" required value="<?php if (isset($user_profile->email)) echo $user_profile->email;?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputPassword3" class="col-md-3 control-label">Password </label>
                      <div class="col-md-4">
                        <input type="password" class="form-control" name ="password" id ="password" placeholder="Password" required>
                        <p class="help-block">At least 8 characters <!--Example block-level help text here.--></p>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputPassword3" class="col-md-3 control-label">Confirm Password </label>
                      <div class="col-md-4">
                        <input type="password" class="form-control" name ="password_confirm" id ="password_confirm" placeholder="Password" required>
                        <p class="help-block">At least 8 characters <!--Example block-level help text here.--></p>
                      </div>
                    </div>
                    <div class="form-group">
                      <label  class="col-md-3 control-label"></label>
                      <div class="col-md-6">
                        <div style="clear:both"></div>
                         <button style ="width: 47%;" id="btn_reg" class="btn btn-primary" type="submit">Register</button>
                       </div>
                    </div>
                    <div class="form-group">
                     <label  class="col-md-3 control-label"></label>
                      <div class="col-md-3">
                         <a href="<?php echo base_url();?>user/registerwith/Facebook" class="btn btn-block btn-social btn-facebook" style=" background-color: #3A5795; color: white; ">
                         <span class="fa fa-facebook"></span> Register with Facebook
                      </a>
                          <a href="<?php echo base_url();?>user/registerwith/Google" class="btn btn-block btn-social btn-google" style=" background-color: #D94A38; color: white; ">
                        <span class="fa fa-google-plus"></span> Register with Google
                      </a>
                       </div>
                    </div>
                    <input type="hidden" name="status" value="<?php if (isset($active)){echo $active;}?>"
                  </fieldset>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>  
    </div>  
</div>
<?php include ("partial/footer.php"); ?>

<script type="text/javascript">

$('document').ready(function()
{ 
  //action="<?php echo base_url();?>user/registration"
  $('#my_form').submit(function(evnt){
    evnt.preventDefault();
    var data = $("#my_form").serialize();
    //alert('in ajax');
    $.ajax({
      
      type : 'POST',
      url  : '<?php echo base_url();?>user/registration',
      data : data  ,
      beforeSend: function()
      {  
        $("#error").fadeOut();
        $("#btn_reg").html('<span class="glyphicon glyphicon-transfer"></span> &nbsp; sending ...');
      },
      success :  function(response)
      { 

        //alert("ok1");
        if(response==="ok"){ 
         
         //alert("ok2");
          $("#btn_reg").html(' Registering ...');
          setTimeout(' window.location.href = "<?php echo base_url();?>user/account"; ',4000);
        }
        else{
         
          $("#error").fadeIn(1000, function(){      
          $("#error").html(response);
          $("#error").css('display', 'block');
          $("#btn_reg").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Register');
         });
        }

      }

    });

  });
});

</script>
