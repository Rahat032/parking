<?php include ("partial/header.php"); ?> 
<div class="main-container">
    <div class="container">
      <div class="row">
        <div class="col-sm-6 login-box">
          <div class="panel panel-default">
            <div class="panel-intro text-center">
              <h2 class="logo-title"> 
                <!-- Original Logo will be placed here  --> 
                <span class="logo-icon"><i><img style=" height: 30px; width: 30px; margin-right: -9px; " src="<?php echo base_url(); ?>assets/ico/apple-touch-icon-144-precomposed.png"></i> </span> REZA<span>PARK </span> </h2>
            </div>
            <div class="panel-body">
              <form role="form" class="form-signin" method="post" id="login-form" accept-charset="utf-8" style="margin-top: -7px;">
                <div class="form-group">
                        <?php if(isset($message)){ ?>
                            <div class="col-md-12" id="infoMessage" style="font-size: 15px;padding: 3px; color: red;font-family: sans-serif; "><?php echo $message;?></div>
                         <?php } else if ($this->session->flashdata('message')!=null) {?>
                                <div class="col-md-12" id="infoMessage" style="font-size: 15px;padding: 3px; color: red;font-family: sans-serif; "><?php echo $this->session->flashdata('message');?></div>
                        <?php } ?>
                </div>
                <div id="error">
                   <!-- error will be shown here ! -->
                </div>
                <div class="form-group">
                  <label for="sender-email" class="control-label">Email:</label>
                  <div class="input-icon"> <i class="icon-user fa"></i>
                      <input name="user_email" id="user_email" type="email"  placeholder="email" class="form-control email" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="user-pass"  class="control-label">Password:</label>
                  <div class="input-icon"> <i class="icon-lock fa"></i>
                    <input name="password"  id="password" type="password"  class="form-control" placeholder="Password" required>
                    
                  </div>
                </div>
                <div class="form-group">
                  <button class="btn btn-primary  btn-block" type="submit" name="btn-login" id="btn-login">Sign in</button>
                </div>
              </form>
            </div>
            <div class="panel-footer">
                <p class="text-center pull-right"> <a href="<?php echo base_url();?>user/forgot_password/"> Lost your password? </a> </p>
              <div style=" clear:both"></div>
            </div>
          </div>
          <div class="login-box-btm text-center">
            <p> Don't have an account? <br>
              <a href="<?php echo base_url();?>user/registration/"><strong>Register Yourself !</strong> </a> </p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /.main-container -->
<?php include ("partial/footer.php"); ?>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>

<script type="text/javascript">

  $('document').ready(function()
{ 
     /* validation */
  $("#login-form").validate({
   rules:
   {
      password: {
       required: true,
      },
      user_email: {
        required: true,
        email: true
      },
    },
    messages:
    {
      password:{
        //required: "please enter your password"
      },
      //user_email: "please enter your email address",
    },
    submitHandler: submitForm 
});  
    /* validation */
    
    /* login submit */
  function submitForm()
  {  
    var data = $("#login-form").serialize();
    //alert(data);
    $.ajax({
    
      type : 'POST',
      url  : '<?php echo base_url();?>user/login',
      data : data  ,
      beforeSend: function()
      {  
        $("#error").fadeOut();
        $("#btn-login").html('<span class="glyphicon glyphicon-transfer"></span> &nbsp; sending ...');
      },
      success :  function(response)
      {      
        if(response==="ok"){ 
         
          $("#btn-login").html(' Signing In ...');
          setTimeout(' window.location.href = "<?php echo base_url();?>user/account"; ',4000);
        }
        else{
         
          $("#error").fadeIn(1000, function(){      
          $("#error").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; '+response+' !</div>');
          $("#btn-login").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Sign In');
         });
        }
     }
   });
    return false;
  }
    /* login submit */
});

</script>



