<?php
if (isset($loginValid) && $loginValid == 1) {
    include ("partial/aheader.php");
} else {
    include ("partial/header.php");
}
?>

<div class="main-container">
    <div class="container">
        <div class="row">

            <div class="col-sm-12 page-content">

                <div class="inner-box">


                    <h3 style=" text-align: center; margin-top: 35px; font-size: 35px; "><b>Terms and Conditions of Use</b></h3>
                    <ul>1.&nbsp;<b>Introduction</b>
                        <li>1.1 &nbsp;  These terms and conditions shall govern your use of our website.</li>
                        <li>1.2 &nbsp;  By using our website, you accept these terms and conditions in full; accordingly, if you disagree with these terms and conditions or any part of these terms and conditions, you must not use our website.</li>
                        <li>1.3 &nbsp;  If you [register with our website, submit any material to our website or use any of our website services], we will ask you to expressly agree to these terms and conditions.</li>
                        <li>1.4  &nbsp; You must be at least [18] years of age to use our website; and by using our website or agreeing to these terms and conditions, you warrant and represent to us that you are at least [18] years of age.</li>
                        <li>1.5 &nbsp;  Our website uses cookies; by using our website or agreeing to these terms and conditions, you consent to our use of cookies in accordance with the terms of our [privacy and cookies policy].
                        </li>
                    </ul>
                    <ul>2.&nbsp; <b>Credit</b>
                        <li>2.1&nbsp;   This document was created using a template from SEQ Legal (http://www.seqlegal.com).
                            You must retain the above credit, unless you purchase a licence to use this document without the credit. You can purchase a licence at: http://www.website-contracts.co.uk/seqlegal-licences.html. Warning: use of this document without the credit, or without purchasing a licence, is an infringement of copyright.</li>
                    </ul>
                    <ul>3.&nbsp; <b>Copyright notice</b>
                        <li>3.1 &nbsp;  Copyright (c) [year(s) of first publication] [full name].</li>
                        <li>3.2 &nbsp;  Subject to the express provisions of these terms and conditions:
                            <ul><li>(a) &nbsp;  we, together with our licensors, own and control all the copyright and other intellectual property rights in our website and the material on our website; and</li>
                                <li>(b) &nbsp;  all the copyright and other intellectual property rights in our website and the material on our website are reserved.</li></ul></li>

                    </ul>
                    <ul>4.&nbsp; <b>Licence to use website</b>
                        <li>4.1 &nbsp;  You may:
                            <ul><li>(a)  &nbsp; view pages from our website in a web browser;</li>
                                <li>(b)&nbsp;   download pages from our website for caching in a web browser;</li>
                                <li>(c) &nbsp;  print pages from our website;</li>
                                <li>(d) &nbsp;  [stream audio and video files from our website; and]</li>
                                <li>(e) &nbsp;  [use [our website services] by means of a web browser,]
                                    subject to the other provisions of these terms and conditions.</li></ul></li>
                        <li>4.2 &nbsp;  Except as expressly permitted by Section 4.1 or the other provisions of these terms and conditions, you must not download any material from our website or save any such material to your computer.</li>
                        <li><li>4.3 &nbsp;  You may only use our website for [your own personal and business purposes], and you must not use our website for any other purposes.</li>
                        <li>4.4 &nbsp;  Except as expressly permitted by these terms and conditions, you must not edit or otherwise modify any material on our website.</li>
                        <li>4.5 &nbsp;  Unless you own or control the relevant rights in the material, you must not:
                            <ul><li>(a)&nbsp;   republish material from our website (including republication on another website);</li>
                                <li>(b) &nbsp;  sell, rent or sub-license material from our website;</li>
                                <li>(c) &nbsp;  show any material from our website in public;</li>
                                <li>(d)  &nbsp; exploit material from our website for a commercial purpose; or</li>
                                <li>(e)  &nbsp; redistribute material from our website.</li></ul></li>
                        <li>4.6 &nbsp;  Notwithstanding Section 4.5, you may redistribute [our newsletter] in [print and electronic form] to [any person].</li>
                        <li>4.7 &nbsp;  We reserve the right to restrict access to areas of our website, or indeed our whole website, at our discretion; you must not circumvent or bypass, or attempt to circumvent or bypass, any access restriction measures on our website.</li>
                    </ul>
                    <ul>5.&nbsp; <b>Acceptable use</b>
                        <li>5.1 &nbsp;  You must not:
                            <ul><li>(a)&nbsp;&nbsp;   use our website in any way or take any action that causes, or may cause, damage to the website or impairment of the performance, availability or accessibility of the website;</li>
                                <li>(b) &nbsp;  use our website in any way that is unlawful, illegal, fraudulent or harmful, or in connection with any unlawful, illegal, fraudulent or harmful purpose or activity;</li>
                                <li>(c) &nbsp;  use our website to copy, store, host, transmit, send, use, publish or distribute any material which consists of (or is linked to) any spyware, computer virus, Trojan horse, worm, keystroke logger, rootkit or other malicious computer software;</li>
                                <li>(d) &nbsp;  conduct any systematic or automated data collection activities (including without limitation scraping, data mining, data extraction and data harvesting) on or in relation to our website without our express written consent;</li>
                                <li>(e) &nbsp;  [access or otherwise interact with our website using any robot, spider or other automated means;]</li>
                                <li>(f) &nbsp;&nbsp;  [violate the directives set out in the robots.txt file for our website; or]</li>
                                <li>(g) &nbsp;  [use data collected from our website for any direct marketing activity (including without limitation email marketing, SMS marketing, telemarketing and direct mailing).]</li></ul></li>
                        <li>5.2 &nbsp;  You must not use data collected from our website to contact individuals, companies or other persons or entities.
                        </li>
                        <li>5.3&nbsp;   You must ensure that all the information you supply to us through our website, or in relation to our website, is [true, accurate, current, complete and non-misleading].
                        </li>
                    </ul>
                    <ul>6.&nbsp; <b>Registration and accounts</b>
                        <li>6.1&nbsp;   To be eligible for an individual account on our website under this Section 6, you must [be at least 18 years of age and resident in the United Kingdom].</li>
                        <li>6.2 &nbsp;  You may register for an account with our website by [completing and submitting the account registration form on our website, and clicking on the verification link in the email that the website will send to you].</li>
                        <li>6.3  &nbsp; You must not allow any other person to use your account to access the website.</li>
                        <li>6.4  &nbsp; You must notify us in writing immediately if you become aware of any unauthorised use of your account.</li>
                        <li>6.5 &nbsp;  You must not use any other person's account to access the website[, unless you have that person's express permission to do so].
                        </li>

                    </ul>
                    <ul>7.&nbsp; <b>User IDs and passwords</b>

                        <li>7.1  &nbsp; If you register for an account with our website, [we will provide you with / you will be asked to choose] a user ID and password.</li>
                        <li>7.2 &nbsp;  Your user ID must not be liable to mislead and must comply with the content rules set out in Section 10; you must not use your account or user ID for or in connection with the impersonation of any person.</li> 
                        <li>7.3 &nbsp;  You must keep your password confidential.</li>
                        <li>7.4 &nbsp;  You must notify us in writing immediately if you become aware of any disclosure of your password.</li>
                        <li>7.5  &nbsp; You are responsible for any activity on our website arising out of any failure to keep your password confidential, and may be held liable for any losses arising out of such a failure.</li>


                    </ul>
                    <ul>8. &nbsp; <b>Cancellation and suspension of account</b>
                        <li>8.1&nbsp;   We may:
                            <ul><li>(a) &nbsp;  [suspend your account;]</li>
                                <li>(b) &nbsp;  [cancel your account; and/or]</li>
                                <li>(c) &nbsp;  [edit your account details,]
                                    at any time in our sole discretion without notice or explanation.</li></ul></li>
                        <li>8.2 &nbsp;  You may cancel your account on our website [using your account control panel on the website].
                        </li>

                    </ul>
                    <ul>9.&nbsp; <b>Your content: licence</b>

                        <li>9.1 &nbsp;  In these terms and conditions, "your content" means all works and materials (including without limitation text, graphics, images, audio material, video material, audio-visual material, scripts, software and files) that you submit to us or our website for storage or publication on, processing by, or transmission via, our website.</li>
                        <li>9.2  &nbsp; You grant to us a [worldwide, irrevocable, non-exclusive, royalty-free licence] to [use, reproduce, store, adapt, publish, translate and distribute your content in any existing or future media / reproduce, store and publish your content on and in relation to this website and any successor website / reproduce, store and, with your specific consent, publish your content on and in relation to this website].</li>
                        <li>9.3 &nbsp;   You grant to us the right to sub-license the rights licensed under Section 9.2.</li>
                        <li>9.4 &nbsp;  You grant to us the right to bring an action for infringement of the rights licensed under Section 9.2.</li>
                        <li>9.5 &nbsp;  You hereby waive all your moral rights in your content to the maximum extent permitted by applicable law; and you warrant and represent that all other moral rights in your content have been waived to the maximum extent permitted by applicable law.</li>
                        <li>9.6 &nbsp;  You may edit your content to the extent permitted using the editing functionality made available on our website.</li>
                        <li>9.7 &nbsp;  Without prejudice to our other rights under these terms and conditions, if you breach any provision of these terms and conditions in any way, or if we reasonably suspect that you have breached these terms and conditions in any way, we may delete, unpublish or edit any or all of your content.

                        </li>
                    </ul>
                    <ul>10. &nbsp;  <b>Your content: rules</b>

                        <li>10.1 &nbsp; You warrant and represent that your content will comply with these terms and conditions.</li>
                        <li>10.2 &nbsp; Your content must not be illegal or unlawful, must not infringe any person's legal rights, and must not be capable of giving rise to legal action against any person (in each case in any jurisdiction and under any applicable law).
                        </li>
                        <li>10.3 &nbsp; Your content, and the use of your content by us in accordance with these terms and conditions, must not:
                            <ul><li>(a) &nbsp;  be libellous or maliciously false;</li>
                                <li>(b) &nbsp;  be obscene or indecent;</li>
                                <li>(c) &nbsp;  infringe any copyright, moral right, database right, trade mark right, design right, right in passing off, or other intellectual property right;</li>
                                <li>(d) &nbsp;  infringe any right of confidence, right of privacy or right under data protection legislation;</li>
                                <li>(e) &nbsp;  constitute negligent advice or contain any negligent statement;</li>
                                <li>(f) &nbsp;  constitute an incitement to commit a crime[, instructions for the commission of a crime or the promotion of criminal activity];</li>
                                <li>(g) &nbsp;  be in contempt of any court, or in breach of any court order;</li>
                                <li>(h) &nbsp;  be in breach of racial or religious hatred or discrimination legislation;</li></li>
                        <li>(i)  &nbsp; be blasphemous;</li>
                        <li>(j) &nbsp;  be in breach of official secrets legislation;</li>
                        <li>(k) &nbsp;  be in breach of any contractual obligation owed to any person;</li>
                        <li>(l) &nbsp;  [depict violence[ in an explicit, graphic or gratuitous manner];]</li>
                        <li>(m) &nbsp;  [be pornographic[, lewd, suggestive or sexually explicit];]</li>
                        <li>(n) &nbsp;  [be untrue, false, inaccurate or misleading;]</li>
                        <li>(o) &nbsp;  [consist of or contain any instructions, advice or other information which may be acted upon and could, if acted upon, cause illness, injury or death, or any other loss or damage;]</li>
                        <li>(p) &nbsp;  [constitute spam;]</li>
                        <li>(q) &nbsp;  [be offensive, deceptive, fraudulent, threatening, abusive, harassing, anti-social, menacing, hateful, discriminatory or inflammatory; or]</li></li>
                        <li>(r) &nbsp;  [cause annoyance, inconvenience or needless anxiety to any person.]
                        </li></ul></li>

                    <ul>
                        <ul>11. &nbsp;  <b>Limited warranties</b>
                            <li>11.1 &nbsp; We do not warrant or represent:
                                <ul><li>(a) &nbsp;  the completeness or accuracy of the information published on our website;</li>
                                    <li>(b) &nbsp;  that the material on the website is up to date; or</li>
                                    <li>(c) &nbsp;  that the website or any service on the website will remain available.</li>
                                </ul></li>
                            <li>11.2 &nbsp; We reserve the right to discontinue or alter any or all of our website services, and to stop publishing our website, at any time in our sole discretion without notice or explanation; and save to the extent expressly provided otherwise in these terms and conditions, you will not be entitled to any compensation or other payment upon the discontinuance or alteration of any website services, or if we stop publishing the website.
                            </li>
                            <li>11.3 &nbsp; To the maximum extent permitted by applicable law and subject to Section 12.1, we exclude all representations and warranties relating to the subject matter of these terms and conditions, our website and the use of our website.
                            </li>
                        </ul>
                        <ul>12.  &nbsp; <b>Limitations and exclusions of liability</b>
                            <ul><li>12.1 &nbsp;  Nothing in a contract under these terms and conditions will:
                                <li>(a)  &nbsp; limit or exclude any liability for death or personal injury resulting from negligence;</li>
                                <li>(b) &nbsp;  limit or exclude any liability for fraud or fraudulent misrepresentation;</li>
                                <li>(c)  &nbsp; limit any liabilities in any way that is not permitted under applicable law; or</li>
                                <li>(d) &nbsp;  exclude any liabilities that may not be excluded under applicable law.</li>
                            </ul></li>
                            <li>12.2 &nbsp;  The limitations and exclusions of liability set out in this Section 12 and elsewhere in a contract under these terms and conditions: 
                                <ul><li>(a) &nbsp;   are subject to Section 12.1; and</li>
                                    <li>(b) &nbsp;  govern all liabilities arising under that contract or relating to the subject matter of that contract, including liabilities arising in contract, in tort (including negligence) and for breach of statutory duty, except to the extent expressly provided otherwise in that contract.</li></ul></li>
                            <li>12.3 &nbsp; To the extent that our website and the information and services on our website are provided free of charge, we will not be liable for any loss or damage of any nature.</li></li>
                            <li>12.4 &nbsp; We will not be liable to you in respect of any losses arising out of any event or events beyond our reasonable control.</li>
                            <li>12.5&nbsp;  We will not be liable to you in respect of any business losses, including (without limitation) loss of or damage to profits, income, revenue, use, production, anticipated savings, business, contracts, commercial opportunities or goodwill.</li>
                            <li>12.6 &nbsp; We will not be liable to you in respect of any loss or corruption of any data, database or software.</li>
                            <li>12.7&nbsp;  We will not be liable to you in respect of any special, indirect or consequential loss or damage.</li>
                            <li>12.8 &nbsp; You accept that we have an interest in limiting the personal liability of our officers and employees and, having regard to that interest, you acknowledge that we are a limited liability entity; you agree that you will not bring any claim personally against our officers or employees in respect of any losses you suffer in connection with the website or these terms and conditions (this will not, of course, limit or exclude the liability of the limited liability entity itself for the acts and omissions of our officers and employees).
                            </li>
                        </ul>
                        <ul>13.  &nbsp; <b>Breaches of these terms and conditions</b>

                            <li>13.1 &nbsp;  Without prejudice to our other rights under these terms and conditions, if you breach these terms and conditions in any way, or if we reasonably suspect that you have breached these terms and conditions in any way, we may: 

                                <ul><li>(a) &nbsp;   send you one or more formal warnings;</li>
                                    <li>(b) &nbsp;   temporarily suspend your access to our website;</li>
                                    <li>(c) &nbsp;   permanently prohibit you from accessing our website;</li>
                                    <li>(d)  &nbsp;  block computers using your IP address from accessing our website;</li>
                                    <li>(e) &nbsp;   contact any or all of your internet service providers and request that they block your access to our website;</li>
                                    <li>(f)  &nbsp;  commence legal action against you, whether for breach of contract or otherwise; and/or</li>
                                    <li>(g) &nbsp;   suspend or delete your account on our website.</li></ul></li>
                            <li>13.2 &nbsp;  Where we suspend or prohibit or block your access to our website or a part of our website, you must not take any action to circumvent such suspension or prohibition or blocking (including without limitation creating and/or using a different account).</li>


                        </ul>
                        <ul>14. &nbsp; <b>Variation</b>
                            <li>14.1&nbsp;  We may revise these terms and conditions from time to time.</li>
                            <li>14.2 &nbsp; [The revised terms and conditions shall apply to the use of our website from the date of publication of the revised terms and conditions on the website, and you hereby waive any right you may otherwise have to be notified of, or to consent to, revisions of these terms and conditions. / We will give you written notice of any revision of these terms and conditions, and the revised terms and conditions will apply to the use of our website from the date that we give you such notice; if you do not agree to the revised terms and conditions, you must stop using our website.]</li>
                            <li>14.3 &nbsp; If you have given your express agreement to these terms and conditions, we will ask for your express agreement to any revision of these terms and conditions; and if you do not give your express agreement to the revised terms and conditions within such period as we may specify, we will disable or delete your account on the website, and you must stop using the website.</li>

                        </ul>
                        <ul>15.  &nbsp; <b>Assignment</b>

                            <li>15.1  You hereby agree that we may assign, transfer, sub-contract or otherwise deal with our rights and/or obligations under these terms and conditions. </li>
                            <li>15.2  You may not without our prior written consent assign, transfer, sub-contract or otherwise deal with any of your rights and/or obligations under these terms and conditions. 
                            </li>

                        </ul>

                        <ul>16.  &nbsp; <b>Severability</b>

                            <li>16.1  &nbsp; If a provision of a contract under these terms and conditions is determined by any court or other competent authority to be unlawful and/or unenforceable, the other provisions will continue in effect.</li>
                            <li>16.2  &nbsp; If any unlawful and/or unenforceable provision of a contract under these terms and conditions would be lawful or enforceable if part of it were deleted, that part will be deemed to be deleted, and the rest of the provision will continue in effect.</li> 

                        </ul>
                        <ul>17.  &nbsp; <b>Third party rights</b>

                            <li>17.1  &nbsp; A contract under these terms and conditions is for our benefit and your benefit, and is not intended to benefit or be enforceable by any third party.</li>
                            <li>17.2   &nbsp;The exercise of the parties' rights under a contract under these terms and conditions is not subject to the consent of any third party.</li>


                        </ul>
                        <ul>18.  &nbsp; <b>Entire agreement</b>

                            <li>18.1 &nbsp; Subject to Section 12.1, these terms and conditions, together with [our privacy and cookies policy], shall constitute the entire agreement between you and us in relation to your use of our website and shall supersede all previous agreements between you and us in relation to your use of our website.</li>


                        </ul>
                        <ul>19.  &nbsp; <b>Law and jurisdiction</b>
                            <li>19.1   &nbsp; A contract under these terms and conditions shall be governed by and construed in accordance with [English law].</li>
                            <li>19.2   &nbsp; Any disputes relating to a contract under these terms and conditions shall be subject to the [exclusive / non-exclusive] jurisdiction of the courts of [England].
                            </li>


                        </ul>
                        <ul>20.  &nbsp; <b>Statutory and regulatory disclosures</b>

                            <li>20.1 &nbsp; We are registered in [trade register]; you can find the online version of the register at [URL], and our registration number is [number].</li>
                            <li>20.2&nbsp;  We are subject to [authorisation scheme], which is supervised by [supervisory authority].</li>
                            <li>20.3 &nbsp; We are registered as [title] with [professional body] in [the United Kingdom] and are subject to [rules], which can be found at [URL].</li>
                            <li>20.4 &nbsp; We subscribe to [code(s) of conduct], which can be consulted electronically at [URL(s)].</li>
                            <li>20.5 &nbsp; Our VAT number is [number].</li>


                        </ul>

                        <ul>21.  &nbsp; <b>Our details</b>

                            <li>21.1 &nbsp;  This website is owned and operated by [name].</li>
                            <li>21.2 &nbsp; We are registered in [England and Wales] under registration number [number], and our registered office is at [address].</li>
                            <li>21.3 &nbsp; Our principal place of business is at [address].</li>
                            <li>21.4 &nbsp; You can contact us by writing to the business address given above, by using our website contact form, by email to [email address] or by telephone on [telephone number].</li>


                        </ul>


                </div>
                <!--/.row-box End--> 


            </div>
            <!--/.page-content--> 
        </div>
        <!--/.row--> 
    </div>
    <!--/.container--> 
</div>
<!-- /.main-container -->















<?php
if (isset($loginValid) && $loginValid == 1) {
    include ("partial/afooter.php");
} else {
    include ("partial/footer.php");
}
?>