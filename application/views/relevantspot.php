<?php include ("partial/aheader.php"); ?> 

<div class="main-container">
      <div class="row" style="margin:auto 0px;">
        
    <div class="col-sm-12 page-content">
            
          <div id ="div1" class="inner-box">
            <h2 class="title-2"><i class="icon-heart-1"></i> Spot List </h2>
            <form class="form-horizontal" id ="form1" role="form" action="<?php echo base_url();?>spot/advancesearch" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                      <div class="form-group">
                        <div class="col-sm-9">
                            <input type="text" class="form-control"  name ="address" id ="address" placeholder="Address" value="<?php if($this->session->userdata("address")!= null) { echo $this->session->userdata("address"); }?>">
                          <span style="color: red" id = "address_error"></span>
                        </div>
                      </div>
                       <div class="form-group">  
                       
                        <div class="col-sm-3">
                            <select name ="spot_type" id="spot_type" class="form-control" style=" font-size: 13px; ">
                               <option value="">Select Spot Type</option> 
                               <option value="driveway">Driveway</option> 
                               <option value="laneway">Laneway</option> 
                               <option value="parking_lot">Parking Lot</option>
                               <option value="garage">Garage</option>
                            </select>
                           </div>
                        
                     
                   
                       
                        <div class="col-sm-3">
                          <input type="text" class="form-control date" name ="from" value="<?php if($this->session->userdata("from")!= null) { echo $this->session->userdata("from");}?>">
                        </div>
                        <div class="col-sm-3">
                          <input type="text" class="form-control date" name ="to" value="<?php if($this->session->userdata("to")!= null) { echo $this->session->userdata("to");}?>">
                        </div>
                         <div class="col-sm-3">
                           <button type="submit" class="btn btn-default">Search Spot</button>
                         </div>
                       </div>
                  
                                <input type="hidden"  name ="latitude" id ="latitude" value="<?= $this->session->userdata("lat"); ?>">
                                <input type="hidden"  name ="longitude" id ="longitude" value="<?= $this->session->userdata("lang"); ?>">
                                <input type="hidden"  name ="latitudeA" id ="latitudeA" value="<?= $this->session->userdata("latitudeA"); ?>">
                                <input type="hidden"  name ="longitudeA" id ="longitudeA" value="<?= $this->session->userdata("longitudeA"); ?>">
                                <input type="hidden"  name ="lata" id ="lata" value="<?php if($this->session->userdata("lata") != null ) { echo$this->session->userdata("lata");} ?>">
                                <input type="hidden"  name ="lngb" id ="lngb" value="<?php if($this->session->userdata("lngb")!= null) { echo$this->session->userdata("lngb"); } ?>">
                                <input type="hidden"  name ="latc" id ="latc" value="<?php if($this->session->userdata("latc")!= null) { echo$this->session->userdata("latc"); } ?>">
                                <input type="hidden"  name ="lngd" id ="lngd" value="<?php if($this->session->userdata("lngd")!= null) { echo$this->session->userdata("lngd");} ?>">
                                <input type="hidden"  name ="zoom" id ="zoom" value="<?php if($this->session->userdata("zoom")!= null) { echo$this->session->userdata("zoom");} ?>">
                                
                                
                         
                    </form>
			<div class="search_part" style="position:relative">
            <div class="row google_map" id ="map_div">
                    <?php echo $map['html']; ?>
            </div>

           <div class="col-sm-4 map_div_main">
                <div class="table-responsive" style="height: 403px;overflow-y: auto;">
				 
				  <table id="addManageTable" class="table table-striped table-bordered add-manage-table table demo search_table" data-filter="#filter" data-filter-text-only="true" >
					<thead>
					  <tr>
						<th>Parking spaces nearby<span class="pull-right"><input type="checkbox" aria-label="...">Unavailable Slots</span></th>
					  </tr>
					</thead>
					<tbody>
					  
					
					  <?php 
						if ( $record_list ) {
							$i=0;
						  foreach( $record_list as $record) {
					  ?>
							 <?php if ($record['active']!=1){?>
							 
								 <tr style=" background-color: gray; "> 
							 <?php } else { ?>
								 <tr style=" background-color: #B4D88E; ">  
							<?php } ?>
						 <td style="width:58%" class="ads-details-td"><div>
							<p><strong> <a href="#"> <?=$record['address'];?></a> </strong></p>
							<p> <strong> Distance </strong>:
								<?= number_format((float)$record['distance'], 2, '.', '');?>Km </p>
                                                        <?php  if(isset($record['hourlyPrice']) and $record['hourlyPrice']!=null) { ?>
                                                        
                                                        <p class="no-margin"><strong>Hourly Price:</strong> $<?=$record['hourlyPrice'];?></p>
                                                       
                                                        <?php } ?>
                                                        <?php  if(isset($record['dailyPrice']) and $record['dailyPrice']!=null) { ?>
                                                      
                                                        <p class="no-margin"><strong>Daily Price:</strong> $<?=$record['dailyPrice'];?></p>
                                                  
                                                        <?php } ?>
                                                        <?php  if(isset($record['weeklyPrice']) and $record['weeklyPrice']!=null) { ?>
                                                 
                                                        <p class="no-margin"><strong>Weekly Price:</strong> $<?=$record['weeklyPrice'];?></p>
                                                  
                                                        <?php } ?>
                                                        <?php  if(isset($record['monthlyPrice']) and $record['monthlyPrice']!=null) { ?>
                                                 
                                                        <p class="no-margin"><strong>Monthly Price:</strong> $<?=$record['monthlyPrice'];?></p>
                                                        <?php } ?>
							
							<p>
								<a target="_blank"  href="<?=base_url();?>spot/detail/<?=$record['id'];?>" class="btn btn-primary btn-sm">View Detail</a>
								<a target="_blank" href="<?=base_url();?>spot/booking/<?=$record['id'];?>" class="btn btn-primary btn-sm">Book this Spot</a> 
							</p>
						   
						  </div></td>
					  </tr>
					  <?php 
						}
					  }
					  ?>
					</tbody>
						  
				  </table>
				</div>
            </div>
           </div>
           
                   
            
            <!--/.row-box End--> 
            
          </div>
        </div>
        <!--/.page-content--> 
      </div>
      <!--/.row--> 
    <!--/.container--> 
  </div>
  <!-- /.main-container -->
  <?php include ("partial/afooter.php"); ?>
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
 
  <?php echo $map['js']; ?>
<script>
  $(function() {
    $( ".date" ).datepicker({dateFormat: "yy-mm-dd"});
    
    $('#form1').submit(function() {
        if ($.trim($("#address").val()) === "") {
                $("#address_error").text('Address is required !');
                return false;
        }
        if ($.trim($("#lata").val()) === "" && $.trim($("#lngb").val()) === "" && $.trim($("#latc").val()) === "" && $.trim($("#lngd").val()) === "") {
            $("#address_error").text('Select address from drop down!');    
            return false;
        }
         
    });
    
   function initialize() {
     var input = document.getElementById('address');
     var autocomplete = new google.maps.places.Autocomplete(input);
     google.maps.event.addListener(autocomplete, 'place_changed', function () {
             
            var place = autocomplete.getPlace();
            document.getElementById('latitudeA').value = place.geometry.location.lat();
            document.getElementById('longitudeA').value = place.geometry.location.lng();
            var bounds = map.getBounds();
            var ne = bounds.getNorthEast(); 
            var sw = bounds.getSouthWest(); 
            $("#lata").val(ne.lat());
            $("#lngb").val(ne.lng());
            $("#latc").val(sw.lat());
            $("#lngd").val(sw.lng());
        });
        
        google.maps.event.addListener(map, 'zoom_changed', function() {
                var bounds = map.getBounds();
                var ne = bounds.getNorthEast(); // LatLng of the north-east corner
                var sw = bounds.getSouthWest(); // LatLng of the south-west corder
                $("#lata").val(ne.lat());
                $("#lngb").val(ne.lng());
                $("#latc").val(sw.lat());
                $("#lngd").val(sw.lng());
                $("#zoom").val(map.getZoom());
                ajaxRequest()
            });
            google.maps.event.addListener(map, 'dragend', function() {
                var bounds = map.getBounds();
                var ne = bounds.getNorthEast(); // LatLng of the north-east corner
                var sw = bounds.getSouthWest(); // LatLng of the south-west corder
                $("#lata").val(ne.lat());
                $("#lngb").val(ne.lng());
                $("#latc").val(sw.lat());
                $("#lngd").val(sw.lng());
                $("#zoom").val(map.getZoom());
                ajaxRequest()
            });    
            
            
    }

    google.maps.event.addDomListener(window, 'load', initialize);
    function ajaxRequest(){
         var data = $("#form1").serialize();
                $.ajax({

                      type : 'POST',
                      url  : '<?php echo base_url();?>spot/getspotresult',
                      data : data  ,
                      dataType: 'json',
                      success :  function(result)
                      {      
                      // alert(result);
                     // console.log(result.record_list[0].id);
                       //alert(result[map]);
                       var res = "";
                        for(var i=0;i<result.record_list.length;i++) 
                        {
                           
                           if (result.record_list[i].active!=1){
                                res += "<tr style=' background-color: gray;'>";
                            } else {
                                res += "<tr style='background-color: #B4D88E;'>" ; 
                            } 
                                res += " <td style='width:58%' class='ads-details-td'><div>";
                                res += " <p><strong> <a href='#'>"+ result.record_list[i].address+ "</a> </strong></p>";
                                var num = Number(result.record_list[i].distance);
                                res += "<p> <strong> Distance </strong>: "+num.toFixed(2)+"Km </p>";
                            if(result.record_list[i].hourlyPrice !=null) { 
                                 res += "<p class='no-margin'><strong>Hourly Price:</strong> $"+result.record_list[i].hourlyPrice+"</p>";
                            } 
                            if(result.record_list[i].dailyPrice !=null) { 
                                 res += "<p class='no-margin'><strong>Daily Price:</strong> $"+result.record_list[i].dailyPrice+"</p>";
                            } 
                            if(result.record_list[i].weeklyPrice !=null) { 
                                 res += "<p class='no-margin'><strong>Weekly Price:</strong> $"+result.record_list[i].weeklyPrice+"</p>";
                            } 
                            if(result.record_list[i].monthlyPrice !=null) { 
                                 res += "<p class='no-margin'><strong>Monthly Price:</strong> $"+result.record_list[i].monthlyPrice+"</p>";
                            } 
                            res += "<p>";
                            res += "<a target='_blank' href='<?=base_url();?>spot/detail/"+result.record_list[i].id+"' class='btn btn-primary btn-sm'>View Detail</a>";
                            res += " <a target='_blank' href='<?=base_url();?>spot/booking/" +result.record_list[i].id+"' class='btn btn-primary btn-sm'>Book this Spot</a>";
                            res += "</p>";
           
                            res += "</div></td>";
                            res += "</tr>";
                             if(result.record_list[i].active != "2"){
                                var icon = "<?=base_url();?>/assets/img/available.png";
                            } else{
                               var icon = "<?=base_url();?>/assets/img/notavailable.png";
                            }
               
                            var latlng = new google.maps.LatLng(result.record_list[i].latitude, result.record_list[i].longitude);
                             var marker = new google.maps.Marker({
                                position: latlng,
                                title: result.record_list[i].address,
                                map: map,
                                icon:icon
                            });
                          
                        }
                      // $('#map_div').html('');
                       $("#addManageTable > tbody > tr").empty();
                       // $('#map_div').html(result.map);
                       $("#addManageTable").append(res);
                       //$('#map_canvas').gmap('clearMarkers');

                      }
                   });
    }
    
  });
  
</script>
