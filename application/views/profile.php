<?php include ("/include/header.php"); ?>

    <!--breadcrumbs start-->
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-4">
                    <h1>User Profile</h1>
                </div>
                <div class="col-lg-8 col-sm-8">
                    <ol class="breadcrumb pull-right">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Pages</a></li>
                        <li class="active">Edit Profile</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->

    <!--container start-->
    <div class="registration-bg">
        <div class="container">

           <form action="<?php echo base_url();?>user/update/<?php echo $userData[0]->id ;?>" class="form-signin wow fadeInUp" method="post" accept-charset="utf-8">      
          
                <h2 class="form-signin-heading">Edit Profile</h2>
                <?php if(isset($message)){ ?>
                <div id="infoMessage"><?php echo $message;?></div>
                <?php } ?>
                <div class="login-wrap">
                    First Name :<input type="text" class="form-control" name ="first_name" value="<?php echo $userData[0]->first_name ;?>" placeholder="First Name" autofocus="" >
                    <input type="text" class="form-control" name ="last_name" value="<?php echo $userData[0]->last_name ;?>" placeholder="Last Name" autofocus="">
                    <input type="text" class="form-control" name ="username" value="<?php echo $userData[0]->username ;?>" placeholder="User Name" autofocus="">
                    <input type="text" class="form-control" name ="email" value="<?php echo $userData[0]->email ;?>" placeholder="Email" autofocus="">
                    <input type="text" class="form-control" name ="phone" value="<?php echo $userData[0]->phone ;?>" placeholder="Phone" autofocus="">
                    <input type="text" class="form-control" name ="address" value="<?php echo $userData[0]->address ;?>" placeholder="Address" autofocus="">
                    <input type="text" class="form-control" name ="city" value="<?php echo $userData[0]->city ;?>" placeholder="City/Town" autofocus="">
                    <input type="password" class="form-control" name ="password" placeholder="Password">
                    <input type="password" class="form-control" name ="password_confirm" placeholder="Re-type Password">
                    
                    <div class="radios">
                        <label class="label_radio col-lg-6 col-sm-6" for="radio">
                            <?php if ($userData[0]->gender == 0) {?>
                            <input name ="gender" id="radio-01" value="0" type="radio"> Male
                            <?php }else {?>
                             <input name ="gender" id="radio-01" value="0" type="radio" checked> Male
                            <?php } ?>
                        </label>
                        <label class="label_radio col-lg-6 col-sm-6" for="radio">
                             <?php if ($userData[0]->gender == 1) {?>
                            <input name ="gender" id="radio-02" value="1" type="radio" checked> Female
                             <?php }else {?>
                             <input name ="gender" id="radio-02" value="1" type="radio"> Female
                              <?php } ?>
                        </label>
                    </div>

                   
                    <button class="btn btn-lg btn-login btn-block" type="submit">Save</button>

                    
                </div>
            </form>

        </div>
     </div>
    <!--container end-->
<?php include ("/include/footer.php"); ?>