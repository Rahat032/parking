<?php include ("partial/header.php"); ?> 
<div class="main-container">
    <div class="container">
      <div class="row">
        <div class="col-sm-7 login-box">
          <div class="panel panel-default">
            <div class="panel-intro text-center">
              <h2 class="logo-title"> 
                <!-- Original Logo will be placed here  --> 
                <span class="logo-icon"><i class="icon icon-search-1 ln-shadow-logo shape-0"></i> </span> REZA<span>PARK </span> </h2>
            </div>
            <div class="panel-body">
              <form role="form" action="<?php echo base_url();?>user/reset_password/<?php echo $code; ?>" class="form-signin wow fadeInUp" method="post" accept-charset="utf-8">
                <div class="form-group">
                        <?php if(isset($message)){ ?>
                            <div class="col-md-12" id="infoMessage" style="font-size: 15px;padding: 3px; color: red;font-family: sans-serif; "><?php echo $message;?></div>
                        <?php } else if ($this->session->flashdata('message')!=null) {?>
                                <div class="col-md-12" id="infoMessage" style="font-size: 15px;padding: 3px; color: red;font-family: sans-serif; "><?php echo $this->session->flashdata('message');?></div>
                        <?php } ?>
                </div>
                 <div class="form-group">
                      <label for="inputPassword3" class="col-md-7 control-label">Password </label>
                      <div class="col-md-12">
                        <input type="password" class="form-control" name ="new" id ="new" placeholder="Password" required>
                        <p class="help-block">At least 8 characters <!--Example block-level help text here.--></p>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputPassword3" class="col-md-7 control-label">Confirm Password </label>
                      <div class="col-md-12">
                        <input type="password" class="form-control" name ="new_confirm" id ="new_confirm" placeholder="Password" required>
                        <p class="help-block">At least 8 characters <!--Example block-level help text here.--></p>
                      </div>
                    </div>
                <div class="form-group">
                  <button class="btn btn-primary  btn-block" type="submit" name="submit">Reset</button>
                </div>
                   <input type="hidden" name="user_id" value="<?php echo $user_id; ?>" id="user_id">
              </form>
            </div>
          </div>
          <div class="login-box-btm text-center">
            <p> Don't have an account? <br>
              <a href="<?php echo base_url();?>user/registration/"><strong>Register Yourself !</strong> </a> </p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /.main-container -->
<?php include ("partial/footer.php"); ?>
