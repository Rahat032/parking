
<?php include ("partial/aheader.php"); ?> 
<div class="main-container">
    <div class="container">
        <div class="row">
            <?php include ("partial/asidebaar.php"); ?> 
            <!--/.page-sidebar-->

            <div class="col-sm-9 page-content">
                <div class="inner-box">
                    <h2 class="title-2"><i class="icon-warning"></i> Close Account </h2>
                    
                    <?php if($can_delete){ ?>
                    <div class="alert alert-danger" role="alert">
                        
                        <strong>Warning!</strong> You will no longer be able to access your account.
                        
                        <p class="col-md-offset-8" >
                            <a href="#" class="btn btn-primary" role="button" onclick="history.go(-1);" > Back </a> 
                            <a href="<?=base_url();?>users/inactivate_user" class="btn btn-danger" role="button"> Proceed </a>
                        </p>
                        
                    </div>
                    <?php }  else { ?>
                    
                    <div class="alert alert-info" role="alert">
                        You can't close account, your spot is rented.  
                        <a href="#" class="alert-link" onclick="history.go(-1);" > Go back ... </a>
                    </div>
                    
                    <?php } ?>

                    <!--/.row-box End--> 

                </div>
            </div>
            <!--/.page-content--> 
        </div>
        <!--/.row--> 
    </div>
    <!--/.container--> 
</div>
<!-- /.main-container -->
<?php include ("partial/afooter.php"); ?>