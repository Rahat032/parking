<?php include ("partial/header.php"); ?> 
<div class="main-container">
    <div class="container">
      <div class="row">
        <div class="col-sm-6 login-box">
          <div class="panel panel-default">
            <div class="panel-intro text-center">
              <h2 class="logo-title"> 
                <!-- Original Logo will be placed here  --> 
                <span class="logo-icon"><i class="icon icon-search-1 ln-shadow-logo shape-0"></i> </span> REZA<span>PARK </span> </h2>
            </div>
            <div class="panel-body">
              <form role="form" action="<?php echo base_url();?>user/forgot_password" class="form-signin wow fadeInUp" method="post" accept-charset="utf-8">
                <div class="form-group">
                        <?php if(isset($message)){ ?>
                            <div class="col-md-12" id="infoMessage" style="font-size: 15px;padding: 3px; color: red;font-family: sans-serif; "><?php echo $message;?></div>
                        <?php } else if ($this->session->flashdata('message')!=null) {?>
                                <div class="col-md-12" id="infoMessage" style="font-size: 15px;padding: 3px; color: red;font-family: sans-serif; "><?php echo $this->session->flashdata('message');?></div>
                        <?php } ?>
                </div>
                <div class="form-group">
                  <label for="sender-email" class="control-label">Email:</label>
                  <div class="input-icon"> <i class="icon-user fa"></i>
                      <input name="identity" id="identity" type="email"  placeholder="email" class="form-control email" required>
                  </div>
                </div>
                <div class="form-group">
                  <button class="btn btn-primary  btn-block" type="submit" name="submit">Reset</button>
                </div>
              </form>
            </div>
          </div>
          <div class="login-box-btm text-center">
            <p> Don't have an account? <br>
              <a href="<?php echo base_url();?>user/registration/"><strong>Register Yourself !</strong> </a> </p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /.main-container -->
<?php include ("partial/footer.php"); ?>
