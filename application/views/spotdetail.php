<?php include ("partial/aheader.php"); ?> 
<div class="main-container">
    <div class="container">
      <div class="row">
        
       <!--- <?php //include ("partial/asidebaar.php"); ?> -->
        <div class="col-sm-12 page-content">
          
          
          
          
          <div class="inner-box">
            <div class="welcome-msg">
            <div id="accordion" class="panel-group">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title"> <a href="#collapseB1"  data-toggle="collapse">Spot Detail</a> </h4>
                </div>
                <div class="panel-collapse collapse in" id="collapseB1">
                  <div class="panel-body">
                    <form class="form-horizontal" role="form" action="<?php echo base_url();?>user/update_spot/<?=$record_list[0]['id'];?>" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                        <div class="row">
							<div class="col-md-4">
							   <img class="img-responsive img-circle" src="<?php if(isset($record_list[0]['image'])){
									 ?><?php echo base_url(); ?>assets/image/<?=$record_list[0]['image'];?><?php } else {?> 
									 <?php echo base_url(); ?>images/add2.jpg<?php } ?>">
							</div>
							<div class="col-md-8">
								<aside class="panel panel-body panel-details">
								  <ul>
									<?php  if(isset($record_list[0]['hourlyPrice']) and $record_list[0]['hourlyPrice']!=null) { ?>
									<li>
									<p class="no-margin"><strong>Hourly Price:</strong> $<?=$record_list[0]['hourlyPrice'];?></p>
									</li>
									<?php } ?>
									<?php  if(isset($record_list[0]['dailyPrice']) and $record_list[0]['dailyPrice']!=null) { ?>
									<li>
									<p class="no-margin"><strong>Daily Price:</strong> $<?=$record_list[0]['dailyPrice'];?></p>
									</li>
									<?php } ?>
									<?php  if(isset($record_list[0]['weeklyPrice']) and $record_list[0]['weeklyPrice']!=null) { ?>
									<li>
									<p class="no-margin"><strong>Weekly Price:</strong> $<?=$record_list[0]['weeklyPrice'];?></p>
									</li>
									<?php } ?>
									<?php  if(isset($record_list[0]['monthlyPrice']) and $record_list[0]['monthlyPrice']!=null) { ?>
									<li>
									<p class="no-margin"><strong>Monthly Price:</strong> $<?=$record_list[0]['monthlyPrice'];?></p>
									</li>
									 <?php } ?>
									<li>
									  <p class="no-margin"><strong>Spot Location:</strong> <?=$record_list[0]['address'];?></p>
									</li>
									<li>
									  <p class="no-margin"><strong>Spot description:</strong> <?=$record_list[0]['spot_discription'];?> </p>
									</li>
									<li>
									  <p class=" no-margin "><strong>Spot available services :</strong>
										<?php if ($record_list[0]['spot_available_service']=="authorized"){ ?>
											Authorized type of vehicle
										<?php } else if ($record_list[0]['spot_available_service']=="invalid") { ?>
											Invalid person
										<?php } else if ($record_list[0]['spot_available_service']=="public") { ?>
											Public transport
										<?php } else { ?>
											Not available
										<?php } ?></p>
									</li>
									<li>
									  <p class="no-margin">
										<strong>Spot owner:</strong>
										 <a href="<?=base_url();?>users/user_detail/<?= $record_list[0]['user_id']?>"><?=$spot_owner;?></a> 
									  </p>
									</li>
								   
								  </ul>
								</aside>
								<div class="row" style="    padding-top: 10px;">
									<div class="col-md-4">
									  <a onclick="getLocation()" class="btn  btn-info">Try a Different Booking Period</a>
									</div>
									<div class="col-md-4">
									  <button type="submit" class="btn btn-default">Book this Spot</button>
									</div>
								</div>
							</div>
                        
                      
                      <input type="hidden"  name ="latitude" id ="latitude" value="<?=$record_list[0]['latitude'];?>">
                      <input type="hidden"  name ="longitude" id ="longitude" value="<?=$record_list[0]['longitude'];?>">
                    </div>
					</form>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">Map</h4>
                </div>
               
                  <div class="panel-body" id="map" style="height: 400px; display : none;">
                  </div>
                
              </div>
              
            </div>
            <!--/.row-box End--> 
            
          </div>
        </div>
        <!--/.page-content--> 
      </div>
      <!--/.row--> 
    </div>
    <!--/.container--> 
  </div>
  <!-- /.main-container -->
<?php include ("partial/afooter.php"); ?>
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<script src="http://maps.google.com/maps/api/js" 
          type="text/javascript"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

  <script>
  $(function() {
    $( ".date" ).datepicker({dateFormat: "yy-mm-dd"});
    
          var address='<?=$record_list[0]['address'];?>';
                        
            var locations = [
                [address,$("#latitude").val(), $("#longitude").val(), 1]
              ];

              var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 12,
                center: new google.maps.LatLng($("#latitude").val(),$("#longitude").val()),
                     travelMode: google.maps.TravelMode.DRIVING,

                mapTypeId: google.maps.MapTypeId.ROADMAP
              });

              var infowindow = new google.maps.InfoWindow();

              var marker, i;

              for (i = 0; i < locations.length; i++) {  
                marker = new google.maps.Marker({
                  position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                  map: map
                });

               google.maps.event.addListener(marker, 'dragend', (function(marker, i) {
                                          return function() {
                                           
                                                jQuery('#latitude').val(this.getPosition().lat());
                                                jQuery('#longitude').val(this.getPosition().lng());
//                                            infowindow.setContent(locations[i][0]);
//                                            infowindow.open(map, marker);
                                          }
                })(marker, i));
              }
              $("#map").css('display', 'block');
                       
                        
                
    });
   
  
  </script>