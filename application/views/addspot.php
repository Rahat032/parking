<?php include ("partial/aheader.php"); ?> 
<div class="main-container">
    <div class="container">
        <div class="row">
            <?php include ("partial/asidebaar.php"); ?> 
            <div class="col-sm-9 page-content">


                <div class="inner-box">
                    <div class="welcome-msg">
                        <div id="accordion" class="panel-group">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"> <a href="#collapseB1"  data-toggle="collapse">Add Spot</a> </h4>
                                </div>
                                <div class="panel-collapse collapse in" id="collapseB1">
                                    <div class="panel-body">
                                        <form id ="form1" class="form-horizontal" role="form" action="<?php echo base_url(); ?>user/addspot" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                                            <div class="form-group">
                                                <?php if ($this->session->flashdata('message') != null) { ?>
                                                    <div class=" col-sm-offset-3 col-md-9" id="infoMessage" style="font-size: 15px;padding: 3px; color: green;font-family: sans-serif; "><?php echo $this->session->flashdata('message'); ?></div>
                                                <?php } ?>
                                            </div>

                                            <div class="form-group">
                                                <label  class="col-sm-3 control-label">Address</label>
                                                <div class="col-sm-9">
                                                    <input type="text" required class="form-control"  name ="address" id ="address" autofocus="autofocus" placeholder="address">
                                                    <span style="color: red" id = "address_error"></span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Image</label>
                                                <div class="col-sm-9">
                                                    <input type="file" name="image" id="image" value="Upload Image">
                                                    <span style=" color: red; ">(max image size <?= $max_size ?>KB with max width <?= $max_width ?>px and max height <?= $max_height ?>px )</span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Spot Description</label>
                                                <div class="col-sm-9">
                                                    <textarea required class="form-control" rows="3" name ="spot_discription" placeholder="Spot Description" ></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Spot Type</label>
                                                <div class="col-sm-9">
                                                    <select name ="spot_type" id="spot_type" class="form-control" style=" font-size: 13px; ">
                                                        <option value="">Select</option> 
                                                        <?php foreach ($spot_type as $record) { ?>
                                                            <?php if ($record['spot_type'] != null) { ?>
                                                                <option value="<?= $record['id']; ?>"><?= $record['spot_type']; ?></option> 
                                                            <?php } ?>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Spot available services</label>
                                                <div class="col-sm-9">
                                                    <select name ="spot_available_service" id="spot_available_service" class="form-control" style=" font-size: 13px; ">
                                                        <option value="">Select</option> 
                                                        <?php foreach ($spot_available_services as $record) { ?>
                                                            <?php if ($record['spot_available_services'] != null) { ?>
                                                                <option value="<?= $record['id']; ?>"><?= $record['spot_available_services']; ?></option> 
                                                            <?php } ?>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Spot Price</label>
                                                <br>
                                                <div class="col-sm-6" ><span style=" color: red" id = "price_checkboxes_error"></span></div>
                                            </div>

                                            <div class="checkbox-group required" > <!-- make a group for all checkboxes -->

                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Enable Hourly Rate</label>
                                                    <div class="col-sm-1"> <input type="checkbox" id="checkH" name="checkH" value=""></div> 
                                                    <div class="col-sm-7">
                                                        <div id = "rangeH"></div>
                                                        <input type = "hidden" id = "valueH"  name = "valueH"/>

                                                    </div>
                                                </div>

                                                <div class="form-group" style=" margin-top: 60px; ">
                                                    <label class="col-sm-3 control-label" >Enable Daily Rate</label>
                                                    <div class="col-sm-1"> <input type="checkbox" id="checkD" name="checkD" value=""></div> 
                                                    <div class="col-sm-7">
                                                        <div id = "rangeD"></div>
                                                        <input type = "hidden" id = "valueD"  name = "valueD"/>
                                                    </div>
                                                </div>

                                                <div class="form-group" style=" margin-top: 60px; ">
                                                    <label class="col-sm-3 control-label" >Enable Weekly Rate</label>
                                                    <div class="col-sm-1"> <input type="checkbox" id="checkW" name="checkW" value=""></div> 
                                                    <div class="col-sm-7">
                                                        <div id = "rangeW"></div>
                                                        <input type = "hidden" id = "valueW"  name = "valueW"/>

                                                    </div>
                                                </div>

                                                <div class="form-group" style=" margin-top: 60px; ">
                                                    <label class="col-sm-3 control-label" >Enable Monthly Rate</label>
                                                    <div class="col-sm-1"> <input type="checkbox" id="checkM" name="checkM" value=""></div> 
                                                    <div class="col-sm-7">
                                                        <div id = "rangeM"></div>
                                                        <input type = "hidden" id = "valueM"  name = "valueM"/>
                                                    </div>
                                                </div>

                                            </div> <!--  end div for group of check boxes  -->

                                            <div class="form-group" style=" margin-top: 60px; ">
                                                <label class="col-sm-3 control-label">From</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control date" name ="from" placeholder="From" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">To</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control date" name ="to" placeholder="To" required >
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-offset-3 col-sm-9"> </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-offset-3 col-sm-9">
                                                    <button type="submit" class="btn btn-default">Add Spot</button>
                                                </div>
                                            </div>
                                            <input type="hidden"  name ="latitude" id ="latitude">
                                            <input type="hidden"  name ="longitude" id ="longitude">
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Map</h4>
                                </div>

                                <div class="panel-body" id="map" style="width: 700px; height: 400px; display : none;">
                                </div>

                            </div>

                        </div>
                        <!--/.row-box End--> 

                    </div>
                </div>
                <!--/.page-content--> 
            </div>
            <!--/.row--> 
        </div>
        <!--/.container--> 
    </div>
    <!-- /.main-container -->
    <?php include ("partial/afooter.php"); ?>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
    <script src="http://maps.google.com/maps/api/js?libraries=places" 
    type="text/javascript"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

    <script>
        $(function () {

            $(".date").datepicker({dateFormat: "yy-mm-dd"});
            //slider code
<?php foreach ($spot_price as $record) { ?>
    <?php if ($record['period'] == "Hourly") { ?>
                    var minrange = <?= intval($record['min']); ?>;
                    var maxrange =<?= intval($record['max']); ?>;
                    var slider1 = document.getElementById('rangeH');

                    noUiSlider.create(slider1, {
                        start: minrange, // Hand1le start position
                        step: 1, // Slider moves in increments of '10'
                        //margin: 20, // Handles must be more than '20' apart
                        connect: 'lower', // Display a colored bar between the handles
                        direction: 'ltr', // Put '0' at the bottom of the slider
                        //orientation: 'horizontal', // Orient the slider horizontal
                        //behaviour: 'tap-drag', // Move handle on tap, bar is draggable
                        range: {// Slider can select '0' to '100'
                            'min': minrange,
                            'max': maxrange
                        },
                        pips: {// Show a scale with the slider
                            mode: 'steps',
                            density: 2
                        }
                    });


                    var valueH = document.getElementById('valueH');

                    slider1.noUiSlider.on('update', function (values, handle) {
                        valueH.value = values[handle];
                    });
                    //
                    // slider for daily
    <?php } elseif ($record['period'] == "Daily") { ?>
                    var minrange = <?= intval($record['min']); ?>;
                    var maxrange =<?= intval($record['max']); ?>;
                    var slider2 = document.getElementById('rangeD');

                    noUiSlider.create(slider2, {
                        start: minrange, // Handle start position
                        step: 1, // Slider moves in increments of '10'
                        //margin: 20, // Handles must be more than '20' apart
                        connect: 'lower', // Display a colored bar between the handles
                        direction: 'ltr', // Put '0' at the bottom of the slider
                        //orientation: 'horizontal', // Orient the slider horizontal
                        //behaviour: 'tap-drag', // Move handle on tap, bar is draggable
                        range: {// Slider can select '0' to '100'
                            'min': minrange,
                            'max': maxrange
                        },
                        pips: {// Show a scale with the slider
                            mode: 'steps',
                            density: 2
                        }
                    });

                    var valueD = document.getElementById('valueD');
                    slider2.noUiSlider.on('update', function (values, handle) {
                        valueD.value = values[handle];
                    });
                    // slider for weekly
    <?php } elseif ($record['period'] == "Weekly") { ?>
                    var minrange = <?= intval($record['min']); ?>;
                    var maxrange =<?= intval($record['max']); ?>;
                    var slider3 = document.getElementById('rangeW');

                    noUiSlider.create(slider3, {
                        start: minrange, // Handle start position
                        step: 10, // Slider moves in increments of '10'
                        //margin: 20, // Handles must be more than '20' apart
                        connect: 'lower', // Display a colored bar between the handles
                        direction: 'ltr', // Put '0' at the bottom of the slider
                        //orientation: 'horizontal', // Orient the slider horizontal
                        //behaviour: 'tap-drag', // Move handle on tap, bar is draggable
                        range: {// Slider can select '0' to '100'
                            'min': minrange,
                            'max': maxrange
                        },
                        pips: {// Show a scale with the slider
                            mode: 'steps',
                            density: 2
                        }
                    });

                    var valueW = document.getElementById('valueW');
                    slider3.noUiSlider.on('update', function (values, handle) {
                        valueW.value = values[handle];
                    });
                    // slider for Monthly
    <?php } elseif ($record['period'] == "Monthly") { ?>
                    var minrange = <?= intval($record['min']); ?>;
                    var maxrange =<?= intval($record['max']); ?>;
                    var slider4 = document.getElementById('rangeM');

                    noUiSlider.create(slider4, {
                        start: minrange, // Handle start position
                        step: 25, // Slider moves in increments of '10'
                        //margin: 20, // Handles must be more than '20' apart
                        connect: 'lower', // Display a colored bar between the handles
                        direction: 'ltr', // Put '0' at the bottom of the slider
                        //orientation: 'horizontal', // Orient the slider horizontal
                        //behaviour: 'tap-drag', // Move handle on tap, bar is draggable
                        range: {// Slider can select '0' to '100'
                            'min': minrange,
                            'max': maxrange
                        },
                        pips: {// Show a scale with the slider
                            mode: 'steps',
                            density: 2
                        }
                    });

                    var valueM = document.getElementById('valueM');
                    slider4.noUiSlider.on('update', function (values, handle) {
                        valueM.value = values[handle];
                    });
    <?php }
}
?>
            //at start disable slider
            slider1.setAttribute('disabled', true);
            slider2.setAttribute('disabled', true);
            slider3.setAttribute('disabled', true);
            slider4.setAttribute('disabled', true);
            $('#checkD').click(function () {
                if ($(this).is(':checked')) {
                    $(this).val("on");
                    slider2.removeAttribute('disabled');

                } else {
                    $(this).val("");
                    slider2.setAttribute('disabled', true);
                }
            });
            $('#checkH').click(function () {
                if ($(this).is(':checked')) {
                    $(this).val("on");
                    slider1.removeAttribute('disabled');

                } else {
                    $(this).val("");
                    slider1.setAttribute('disabled', true);
                }
            });
            $('#checkW').click(function () {
                if ($(this).is(':checked')) {
                    $(this).val("on");
                    slider3.removeAttribute('disabled');

                } else {
                    $(this).val("");
                    slider3.setAttribute('disabled', true);
                }
            });
            $('#checkM').click(function () {
                if ($(this).is(':checked')) {
                    $(this).val("on");
                    slider4.removeAttribute('disabled');

                } else {
                    $(this).val("");
                    slider4.setAttribute('disabled', true);
                }
            });
            //

            $('#form1').submit(function () {

                if ($('div.checkbox-group.required :checkbox:checked').length == 0) {

                    $("#price_checkboxes_error").text('    Select any of the price method for spot !');
                    scroll(0, 0);
                    return false;
                }

                if ($.trim($("#address").val()) === "") {
                    $("#address_error").text('Address is required !');
                    scroll(0, 0);
                    return false;
                }


            });
            function initialize() {

                var input = document.getElementById('address');

                var autocomplete = new google.maps.places.Autocomplete(input);
                var mapOptions = {
                    zoom: <?php echo $zoom; ?>

                };
                var map = new google.maps.Map(document.getElementById("map"), mapOptions);
                var allowedBounds = new google.maps.LatLngBounds(
                        new google.maps.LatLng(85, -180), // top left corner of map
                        new google.maps.LatLng(-85, 180)            // bottom right corner
                        );


                $("#map").css('display', 'block');


                google.maps.event.addListener(autocomplete, 'place_changed', function () {

                    var place = autocomplete.getPlace();
                    document.getElementById('latitude').value = place.geometry.location.lat();
                    document.getElementById('longitude').value = place.geometry.location.lng();

                    if (place.geometry) {
                        map.panTo(place.geometry.location);

                    }
                    var myLatLng = {lat: place.geometry.location.lat(), lng: place.geometry.location.lng()};
                    // clearMarkers();
                    var marker = new google.maps.Marker({
                        position: myLatLng,
                        draggable: true,
                        map: map
                    });

                });

            }

            google.maps.event.addDomListener(window, 'load', initialize);
        });
    </script>
