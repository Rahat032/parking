<?php
if (isset($loginValid) && $loginValid == 1) {
    include ("partial/aheader.php");
} else {
    include ("partial/header.php");
}
?>

<div class="main-container">
    <div class="container">
        <div class="row">

            <div class="col-sm-12 page-content">

                <div class="inner-box">


                    <h3 style=" text-align: center; margin-top: 35px; font-size: 35px; "><b>Privacy Policy</b></h3>
                    <ul>1.&nbsp;<b>Introduction</b>
                        <li>1.1&nbsp;   We are committed to safeguarding the privacy of our website visitors; in this policy we explain how we will treat your personal information.</li>
                        <li>1.2&nbsp;   [We will ask you to consent to our use of cookies in accordance with the terms of this policy when you first visit our website. / By using our website and agreeing to this policy, you consent to our use of cookies in accordance with the terms of this policy.]</li>
                    </ul>
                    <ul>2.&nbsp; <b>Credit</b>
                        <li>2.1&nbsp;   This document was created using a template from SEQ Legal (http://www.seqlegal.com).
                            You must retain the above credit, unless you purchase a licence to use this document without the credit. You can purchase a licence at: http://www.website-contracts.co.uk/seqlegal-licences.html. Warning: use of this document without the credit, or without purchasing a licence, is an infringement of copyright.</li>
                    </ul>
                    <ul>3.&nbsp; <b>Collecting personal information</b>
                        <li><ul>3.1&nbsp;   We may collect, store and use the following kinds of personal information: 
                                <li>(a)&nbsp;   [information about your computer and about your visits to and use of this website (including [your IP address, geographical location, browser type and version, operating system, referral source, length of visit, page views and website navigation paths]);]</li>
                                <li>(b) &nbsp;  [information that you provide to us when registering with our website (including [your email address]);]</li>
                                <li>(c)&nbsp;   [information that you provide when completing your profile on our website (including [your name, profile pictures, gender, date of birth, relationship status, interests and hobbies, educational details and employment details]);]
                                </li>
                                <li>(d) &nbsp;  [information that you provide to us for the purpose of subscribing to our email notifications and/or newsletters (including [your name and email address]);]</li>
                                <li>(e) &nbsp;  [information that you provide to us when using the services on our website, or that is generated in the course of the use of those services (including [the timing, frequency and pattern of service use]);]</li>
                                <li>(f)&nbsp;  [information relating to any purchases you make of our [goods / services / goods and/or services] or any other transactions that you enter into through our website (including [your name, address, telephone number, email address and card details]);]</li>
                                <li>(g)&nbsp;  [information that you post to our website for publication on the internet (including [your user name, your profile pictures and the content of your posts]);]</li>
                                <li>(h)&nbsp;  [information contained in or relating to any communication that you send to us or send through our website (including [the communication content and metadata associated with the communication]);]</li>
                                <li>(i) &nbsp;  [any other personal information that you choose to send to us; and]
                                <li>(j)   [[provide details of other personal information collected].]</li></ul></li>
                        <li>3.2&nbsp;   Before you disclose to us the personal information of another person, you must obtain that person's consent to both the disclosure and the processing of that personal information in accordance with this policy.</li>
                    </ul>
                    <ul>4.&nbsp; <b>Using personal information</b>
                        <li>4.1&nbsp;   Personal information submitted to us through our website will be used for the purposes specified in this policy or on the relevant pages of the website.</li>
                        <li><ul>4.2 &nbsp;  We may use your personal information to: 
                                <li>(a) &nbsp;  [administer our website and business;]</li>
                                <li>(b)  &nbsp; [personalise our website for you;]</li>
                                <li>(c) &nbsp;  [enable your use of the services available on our website;]</li>
                                <li>(d)  &nbsp; [send you goods purchased through our website;]</li>
                                <li>(e) &nbsp;  [supply to you services purchased through our website;]</li>
                                <li>(f) &nbsp;  [send statements, invoices and payment reminders to you, and collect payments from you;]</li>
                                <li>(g) &nbsp;  [send you non-marketing commercial communications;]</li>
                                <li>(h) &nbsp;  [send you email notifications that you have specifically requested;]</li>
                                <li>(i) &nbsp;  [send you our email newsletter, if you have requested it (you can inform us at any time if you no longer require the newsletter);]</li>
                                <li>(j) &nbsp;  [send you marketing communications relating to our business [or the businesses of carefully-selected third parties] which we think may be of interest to you, by post or, where you have specifically agreed to this, by email or similar technology (you can inform us at any time if you no longer require marketing communications);]</li>
                                <li>(k) &nbsp;  [provide third parties with statistical information about our users (but those third parties will not be able to identify any individual user from that information);]</li>
                                <li>(l) &nbsp;  [deal with enquiries and complaints made by or about you relating to our website;]</li>
                                <li>(m) &nbsp;  [keep our website secure and prevent fraud;]</li>
                                <li>(n)  &nbsp; [verify compliance with the terms and conditions governing the use of our website [(including monitoring private messages sent through our website private messaging service)]; and]</li>
                                <li>(o) &nbsp;  [[other uses].]</li></ul></li>
                        <li>4.3 &nbsp;  If you submit personal information for publication on our website, we will publish and otherwise use that information in accordance with the licence you grant to us.</li>
                        <li>4.4 &nbsp;  Your privacy settings can be used to limit the publication of your information on our website, and can be adjusted using privacy controls on the website.</li>
                        <li>4.5 &nbsp;  We will not, without your express consent, supply your personal information to any third party for the purpose of their or any other third party's direct marketing.</li>
                        <li>4.6 &nbsp;  All our website financial transactions are handled through our payment services provider, [PSP name]. You can review the provider's privacy policy at [URL]. We will share information with our payment services provider only to the extent necessary for the purposes of processing payments you make via our website, refunding such payments and dealing with complaints and queries relating to such payments and refunds.</li></ul>
                    <ul>5.&nbsp; <b>Disclosing personal information</b>
                        <li>5.1&nbsp;   We may disclose your personal information to [any of our employees, officers, insurers, professional advisers, agents, suppliers or subcontractors] insofar as reasonably necessary for the purposes set out in this policy.</li>
                        <li>5.2&nbsp;   We may disclose your personal information to any member of our group of companies (this means our subsidiaries, our ultimate holding company and all its subsidiaries) insofar as reasonably necessary for the purposes set out in this policy.</li>
                        <li><ul>5.3&nbsp;   We may disclose your personal information: 
                                <li>(a) &nbsp;  to the extent that we are required to do so by law;</li>
                                <li>(b) &nbsp;  in connection with any ongoing or prospective legal proceedings;</li>
                                <li>(c) &nbsp;  in order to establish, exercise or defend our legal rights (including providing information to others for the purposes of fraud prevention and reducing credit risk);</li>
                                <li>(d) &nbsp;  [to the purchaser (or prospective purchaser) of any business or asset that we are (or are contemplating) selling; and]</li>
                                <li>(e) &nbsp;  [to any person who we reasonably believe may apply to a court or other competent authority for disclosure of that personal information where, in our reasonable opinion, such court or authority would be reasonably likely to order disclosure of that personal information.]</li></ul></li>
                        <li>5.4  &nbsp; Except as provided in this policy, we will not provide your personal information to third parties.</li></ul>
                    <ul>6.&nbsp; <b>International data transfers</b>
                        <li>6.1 &nbsp;  Information that we collect may be stored and processed in and transferred between any of the countries in which we operate in order to enable us to use the information in accordance with this policy.</li>
                        <li>6.2 &nbsp;  Information that we collect may be transferred to the following countries which do not have data protection laws equivalent to those in force in the European Economic Area: [the United States of America, Russia, Japan, China and India].
                        </li>
                        <li>6.3&nbsp;   Personal information that you publish on our website or submit for publication on our website may be available, via the internet, around the world. We cannot prevent the use or misuse of such information by others.</li>
                        <li>6.4 &nbsp;  You expressly agree to the transfers of personal information described in this Section 6.</li></ul>
                    <ul>7.&nbsp; <b>Retaining personal information</b>
                        <li>7.1 &nbsp;  This Section 7 sets out our data retention policies and procedure, which are designed to help ensure that we comply with our legal obligations in relation to the retention and deletion of personal information.</li>
                        <li>7.2  &nbsp; Personal information that we process for any purpose or purposes shall not be kept for longer than is necessary for that purpose or those purposes.</li>
                        <li><ul>7.3 &nbsp;  Without prejudice to Section 7.2, we will usually delete personal data falling within the categories set out below at the date/time set out below: </li>
                        <li>(a)  &nbsp; [personal data type] will be deleted [date/time; and]</li>
                        <li>(b) &nbsp;  [repeat as necessary.]</li></ul></li>
                    <li><ul>7.4  &nbsp;&nbsp; Notwithstanding the other provisions of this Section 7, we will retain documents (including electronic documents) containing personal data: </li>
                    <li>(a) &nbsp;  to the extent that we are required to do so by law;</li>
                    <li>(b)  &nbsp; if we believe that the documents may be relevant to any ongoing or prospective legal proceedings; and</li>
                    <li>(c) &nbsp;  in order to establish, exercise or defend our legal rights (including providing information to others for the purposes of fraud prevention and reducing credit risk).</li></ul></li></ul>
                    <ul>8. &nbsp; <b>Security of personal information</b>
                        <li>8.1  &nbsp;  We will take reasonable technical and organisational precautions to prevent the loss, misuse or alteration of your personal information.</li>
                        <li>8.2  &nbsp;  We will store all the personal information you provide on our secure (password- and firewall-protected) servers.</li>
                        <li>8.3  &nbsp;  All electronic financial transactions entered into through our website will be protected by encryption technology.</li>
                        <li>8.4  &nbsp;  You acknowledge that the transmission of information over the internet is inherently insecure, and we cannot guarantee the security of data sent over the internet.</li>
                        <li>8.5  &nbsp;  You are responsible for keeping the password you use for accessing our website confidential; we will not ask you for your password (except when you log in to our website).</li></ul>
                    <ul>9.&nbsp; <b>Amendments</b>
                        <li>9.1&nbsp;   We may update this policy from time to time by publishing a new version on our website.</li>
                        <li>9.2 &nbsp;  You should check this page occasionally to ensure you are happy with any changes to this policy.</li>
                        <li>9.3 &nbsp;  We may notify you of changes to this policy [by email or through the private messaging system on our website].</li>
                    </ul>
                    <ul>10. &nbsp;  <b>Your rights</b>
                        <li><ul>10.1&nbsp;&nbsp;  You may instruct us to provide you with any personal information we hold about you; provision of such information will be subject to: 
                                <li>(a)&nbsp;   the payment of a fee (currently fixed at GBP 10); and</li>
                                <li>(b) &nbsp;  the supply of appropriate evidence of your identity [(for this purpose, we will usually accept a photocopy of your passport certified by a solicitor or bank plus an original copy of a utility bill showing your current address)].
                                </li></ul></li>
                        <li>10.2&nbsp;  We may withhold personal information that you request to the extent permitted by law.</li>
                        <li>10.3&nbsp;  You may instruct us at any time not to process your personal information for marketing purposes.</li>
                        <li>10.4&nbsp;  In practice, you will usually either expressly agree in advance to our use of your personal information for marketing purposes, or we will provide you with an opportunity to opt out of the use of your personal information for marketing purposes.</li><ul>
                            <ul>11. &nbsp;  <b>Third party websites</b>
                                <li>11.1 &nbsp; Our website includes hyperlinks to, and details of, third party websites.</li>
                                <li>11.2 &nbsp; We have no control over, and are not responsible for, the privacy policies and practices of third parties.
                                </li>
                            </ul>
                            <ul>12.  &nbsp; <b>Updating information</b>
                                <li>12.1 &nbsp; Please let us know if the personal information that we hold about you needs to be corrected or updated.
                                </li>
                            </ul>
                            <ul>13.  &nbsp; <b>Cookies</b>
                                <li>13.1 &nbsp; Our website uses cookies.</li>
                                <li>13.2&nbsp;  A cookie is a file containing an identifier (a string of letters and numbers) that is sent by a web server to a web browser and is stored by the browser. The identifier is then sent back to the server each time the browser requests a page from the server.</li>
                                <li>13.3 &nbsp; Cookies may be either "persistent" cookies or "session" cookies: a persistent cookie will be stored by a web browser and will remain valid until its set expiry date, unless deleted by the user before the expiry date; a session cookie, on the other hand, will expire at the end of the user session, when the web browser is closed. </li>
                                <li>13.4 &nbsp; Cookies do not typically contain any information that personally identifies a user, but personal information that we store about you may be linked to the information stored in and obtained from cookies.</li>
                                <li>13.5 &nbsp; We use [only session cookies / only persistent cookies / both session and persistent cookies] on our website.</li>
                                <li><ul>13.6 &nbsp; The names of the cookies that we use on our website, and the purposes for which they are used, are set out below: </li>
                                <li>(a) &nbsp;  we use [cookie name] on our website to [recognise a computer when a user visits the website / track users as they navigate the website / enable the use of a shopping cart on the website / improve the website's usability / analyse the use of the website / administer the website / prevent fraud and improve the security of the website / personalise the website for each user / target advertisements which may be of particular interest to specific users / [describe purpose(s)]];</li>
                                <li>(b)&nbsp;   [repeat as necessary.]</li></ul></li>
                            <li><ul>13.7 &nbsp; Most browsers allow you to refuse to accept cookies; for example: </li>
                            <li>(a) &nbsp;  in Internet Explorer (version 11) you can block cookies using the cookie handling override settings available by clicking "Tools", "Internet Options", "Privacy" and then "Advanced";</li>
                            <li>(b) &nbsp;  in Firefox (version 39) you can block all cookies by clicking "Tools", "Options", "Privacy", selecting "Use custom settings for history" from the drop-down menu, and unticking "Accept cookies from sites"; and</li>
                            <li>(c) &nbsp;  in Chrome (version 44), you can block all cookies by accessing the "Customise and control" menu, and clicking "Settings", "Show advanced settings" and "Content settings", and then selecting "Block sites from setting any data" under the "Cookies" heading.</li></ul></li>
                        <li>13.8&nbsp;  Blocking all cookies will have a negative impact upon the usability of many websites.</li>
                        <li>13.9 &nbsp; If you block cookies, you will not be able to use all the features on our website.</li>
                        <li><ul>13.10&nbsp; You can delete cookies already stored on your computer; for example:</li>
                        <li>(a) &nbsp;  in Internet Explorer (version 11), you must manually delete cookie files (you can find instructions for doing so at http://windows.microsoft.com/en-gb/internet-explorer/delete-manage-cookies#ie=ie-11);</li></li>
                        <li>(b) &nbsp;  in Firefox (version 39), you can delete cookies by clicking "Tools", "Options" and "Privacy", then selecting "Use custom settings for history" from the drop-down menu, clicking "Show Cookies", and then clicking "Remove All Cookies"; and</li>
                        <li>(c) &nbsp;  in Chrome (version 44), you can delete all cookies by accessing the "Customise and control" menu, and clicking "Settings", "Show advanced settings" and "Clear browsing data", and then selecting "Cookies and other site and plug-in data" before clicking "Clear browsing data".</li></ul></li>
                    <li>13.11&nbsp; Deleting cookies will have a negative impact on the usability of many websites.</li></ul>
                    <ul>14. &nbsp; <b> Data protection registration</b>
                        <li>14.1 &nbsp; We are registered as a data controller with the UK Information Commissioner's Office.</li>
                        <li>14.2 &nbsp; Our data protection registration number is [number].</li></ul>
                    <ul>15.  &nbsp; <b>Our details</b>
                        <li>15.1 &nbsp; This website is owned and operated by [name].</li>
                        <li>15.2 &nbsp; We are registered in [England and Wales] under registration number [number], and our registered office is at [address].</li>
                        <li>15.3 &nbsp; Our principal place of business is at [address].</li>
                        <li>15.4 &nbsp; You can contact us by writing to the business address given above, by using our website contact form, by email to [email address] or by telephone on [telephone number].</li></ul>








                </div>
                <!--/.row-box End--> 


            </div>
            <!--/.page-content--> 
        </div>
        <!--/.row--> 
    </div>
    <!--/.container--> 
</div>
<!-- /.main-container -->















<?php
if (isset($loginValid) && $loginValid == 1) {
    include ("partial/afooter.php");
} else {
    include ("partial/footer.php");
}
?>