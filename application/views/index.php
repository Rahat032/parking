<?php include ("partial/header.php"); ?>

<div class="intro">
    <div class="dtable hw100">
      <div class="dtable-cell hw100">
        <div class="container text-center">
       <h1 class="intro-title animated fadeInDown"> Find Classified Ads  </h1>
<p class="sub animateme fittext3 animated fadeIn"> Find local classified ads on bootclassified in  Minutes </p>
          
          <div class="row search-row animated fadeInUp">
              <div class="col-lg-4 col-sm-4 search-col relative locationicon">
               <i class="icon-location-2 icon-append"></i>
                <input type="text" name="country" id="autocomplete-ajax"  class="form-control locinput input-rel searchtag-input has-icon" placeholder="City/Zipcode..." value="">

              </div>
              <div class="col-lg-4 col-sm-4 search-col relative"> <i class="icon-docs icon-append"></i>
                <input type="text" name="ads"  class="form-control has-icon" placeholder="I'm looking for a ..." value="">
              </div>
              <div class="col-lg-4 col-sm-4 search-col">
                <button class="btn btn-primary btn-search btn-block"><i class="icon-search"></i><strong>Find</strong></button>
              </div>
            </div>
          
        </div>
      </div>
    </div>
  </div>
  <!-- /.intro -->
<div class="main-container">
    <div class="container">
        <div class="row">
            <div class="inner-box relative">
                <div class="row">
                    <div class="col-md-5">
                        <div>
                            <h3 class="title-2"> <i class="icon-location-2"></i> Popular locations  </h3>
                            <div class="row">   <ul class="cat-list col-xs-6">
                                    <li> <a href="category.html">Atlanta</a></li>
                                    <li> <a href="category.html">Wichita </a></li>
                                    <li> <a href="category.html"> Anchorage </a></li>
                                    <li> <a href="category.html"> Dallas  </a></li>
                                    <li> <a href="category.html"> New York  </a></li>
                                    <li> <a href="category.html">Santa Ana/Anaheim </a></li>
                                    <li> <a href="category.html"> Miami  </a></li>
                                    <li> <a href="category.html">Los Angeles</a></li>
                                </ul>

                                <ul class="cat-list cat-list-border col-xs-6">
                                    <li> <a href="category.html">Virginia Beach  </a></li>
                                    <li> <a href="category.html"> San Diego   </a></li>
                                    <li> <a href="category.html">Boston </a></li>
                                    <li> <a href="category.html">Houston</a></li>
                                    <li> <a href="category.html">Salt Lake City </a></li>
                                    <li> <a href="category.html">San Francisco  </a></li>
                                    <li> <a href="category.html">Tampa   </a></li>
                                    <li> <a href="category.html"> Washington DC   </a></li>

                                </ul></div>

                        </div>
                    </div>
                    <div class="col-md-7 ">
                        <h3 class="title-2"> <i class="icon-search-1"></i> Popular Makes </h3>
                        <div class="row">
                            <ul class="cat-list col-md-4 col-xs-4 col-xxs-6">
                                <li> <a href="category.html">free </a></li>
                                <li> <a href="category.html">furniture </a></li>
                                <li> <a href="category.html">general </a></li>
                                <li> <a href="category.html">household </a></li>
                                <li> <a href="category.html">jewelry </a></li>
                                <li> <a href="category.html">materials </a></li>
                                <li> <a href="category.html">sporting </a></li>
                                <li> <a href="category.html">tickets </a></li>
                            </ul>
                            <ul class="cat-list col-md-4 col-xs-4 col-xxs-6">
                                <li> <a href="category.html">tools </a></li>
                                <li> <a href="category.html">wanted </a></li>
                                <li> <a href="category.html">cell phones </a></li>
                                <li> <a href="category.html">clothes+acc </a></li>
                                <li> <a href="category.html">collectibles </a></li>
                                <li> <a href="category.html">electronics </a></li>
                                <li> <a href="category.html">farm+garden </a></li>
                                <li> <a href="category.html">garage sale </a></li>
                            </ul>
                            <ul class="cat-list col-md-4 col-xs-4 col-xxs-6">
                                <li> <a href="category.html">heavy equip </a></li>
                                <li> <a href="category.html">motorcycles </a></li>
                                <li> <a href="category.html">music instr </a></li>
                                <li> <a href="category.html">photo+video </a></li>
                                <li> <a href="category.html">appliances </a></li>
                                <li> <a href="category.html">Land </a></li>
                                <li> <a href="category.html">arts+crafts </a></li>
                                <li> <a href="category.html">auto parts </a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.main-container -->
<?php include ("partial/footer.php"); ?>
