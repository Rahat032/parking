<?php include ("partial/aheader.php"); ?> 
<div class="main-container">
  <div class="container">
      <div class="row">
        <?php include ("partial/asidebaar.php"); ?> 
        <!--/.page-sidebar-->
        
        <div class="col-sm-9 page-content">
          <div class="inner-box">
          <div class="row">
            <div class="col-md-5 col-xs-4 col-xxs-12">
              <h3 class="no-padding text-center-480 useradmin"><a href=""><img style=" height: 70px; width: 70px;" class="userImg" src="<?php if(isset($user[0]['image'])){
                   ?><?php echo base_url(); ?>assets/user/image/<?=$user[0]['image'];?><?php } else {?> 
                   <?php echo base_url(); ?>images/user.jpg<?php } ?>"> <?=$user[0]['first_name'];?>  <?=$user[0]['last_name'];?> </a> </h3> <p> Last Login: Today </p> 
            </div>
          </div>
          </div>
        </div>
        <!--   table start here   -->
        <div class="col-sm-9 page-content">
        <div class="inner-box">
            <h2 class="title-2"><i class="icon-heart-1"></i> Spot List </h2>
            <div class="table-responsive">
             
              <table id="addManageTable" class="table table-striped table-bordered add-manage-table table demo" data-filter="#filter" data-filter-text-only="true" >
                <thead>
                  <tr>
                    <th>Adress</th>
                    <th>Price</th>
                    <th >Actions</th>
                  </tr>
                </thead>
                <tbody>
                  <!-- <tr>
                     <tbody> -->
                  <?php 
                    if ( $spots ) {
                      $i = 0;
                      foreach( $spots as $spot) {
                  ?>
                  <tr>
                    <td><?=$spot['address'];?></td>
                    <td>
                      <ul>
                        <?php  if(isset($spot['hourlyPrice']) and $spot['hourlyPrice']!=null) { ?>
                        <li>
                          <p class="no-margin"><strong>Hourly Price:</strong> $<?=$spot['hourlyPrice'];?></p>
                        </li>
                        <?php } ?>
                        <?php  if(isset($spot['dailyPrice']) and $spot['dailyPrice']!=null) { ?>
                        <li>
                          <p class="no-margin"><strong>Daily Price:</strong> $<?=$spot['dailyPrice'];?></p>
                        </li>
                        <?php } ?>
                        <?php  if(isset($spot['weeklyPrice']) and $spot['weeklyPrice']!=null) { ?>
                        <li>
                          <p class="no-margin"><strong>Weekly Price:</strong> $<?=$spot['weeklyPrice'];?></p>
                        </li>
                        <?php } ?>
                        <?php  if(isset($spot['monthlyPrice']) and $spot['monthlyPrice']!=null) { ?>
                        <li>
                          <p class="no-margin"><strong>Monthly Price:</strong> $<?=$spot['monthlyPrice'];?></p>
                        </li>
                        <?php } ?>
                      </ul>
                    </td>
                    <td>
                      <a href="<?=base_url();?>spot/detail/<?=$spot['id'];?>" class="btn btn-primary btn-sm">View</a>
                    </td>
                  </tr>
                  <?php
                      }
                    }
                  ?>

                </tbody>
                      
              </table>
            </div>
            <!--/.row-box End--> 
            
          </div>
        </div>
        <!--   table ends    -->

        <?php $this->load->view('reviews_view'); ?>

        <!-- reviews area started here -->

        <!--/.page-content--> 
      </div>
      <!--/.row--> 
  </div>
  <!--/.container--> 
</div>
<!-- /.main-container -->
<?php include ("partial/afooter.php"); ?>