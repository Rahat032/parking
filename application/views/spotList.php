<?php include ("partial/aheader.php"); ?> 
<div class="main-container">
    <div class="container">
      <div class="row">
          <?php include ("partial/asidebaar.php"); ?> 
        <!--/.page-sidebar-->
        
        <div class="col-sm-9 page-content">
          <div class="inner-box">
            <h2 class="title-2"><i class="icon-heart-1"></i> Spot List </h2>
            <div class="table-responsive">
             
              <table id="addManageTable" class="table table-striped table-bordered add-manage-table table demo" data-filter="#filter" data-filter-text-only="true" >
                <thead>
                  <tr>
                    <th>Address</th>
                    <th>From</th>
                    <th>To</th>
                    <th >Status</th>
                    <th >Actions</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                     <tbody>
                  <?php 
                    if ( $record_list ) {
                      $i = 0;
                      foreach( $record_list as $record) {
                  ?>
                  <tr>
                    <td><?=$record['address'];?></td>
                    <td><?=$record['dateFrom'];?></td>
                    <td><?=$record['dateTo'];?></td>
                    <td>
                       <?php if($record['active']==1) { ?>
                       <a href="<?=base_url();?>user/status/<?=$record['id'];?>" class="btn btn-info btn-sm">
			Click to Disable</a>
                       <?php } else { ?>
                         <a href="<?=base_url();?>user/status/<?=$record['id'];?>" class="btn btn-primary btn-sm">
			Click to Active</a>
                       <?php } ?>
                    
                    </td>
                    <td>
			<a href="<?=base_url();?>user/edit_spot/<?=$record['id'];?>" class="btn btn-primary btn-sm">
			Edit</a>
                        <a href="<?=base_url();?>user/delete_spot/<?=$record['id'];?>" class="btn btn-danger btn-sm">
			Delete</a>
		    </td>
                  </tr>
                  <?php 
                    }
                  }
                  ?>
                </tbody>
                      
              </table>
            </div>
            <!--/.row-box End--> 
            
          </div>
        </div>
        <!--/.page-content--> 
      </div>
      <!--/.row--> 
    </div>
    <!--/.container--> 
  </div>
  <!-- /.main-container -->
  <?php include ("partial/afooter.php"); ?>