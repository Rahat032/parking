<?php include ("partial/aheader.php"); ?> 
<div class="main-container">
    <div class="container">
      <div class="row">
        
        <?php include ("partial/asidebaar.php"); ?> 
        <div class="col-sm-9 page-content">
          <div class="inner-box">
          <div class="row">
            <div class="col-md-5 col-xs-4 col-xxs-12">
              <h3 class="no-padding text-center-480 useradmin"><a href=""><img style=" height: 70px; width: 70px;" class="userImg" src="<?php if(isset($record_list[0]['image'])){
                   ?><?php echo base_url(); ?>assets/user/image/<?=$record_list[0]['image'];?><?php } else {?> 
                   <?php echo base_url(); ?>images/user.jpg<?php } ?>"> <?=$record_list[0]['first_name'];?>  <?=$record_list[0]['last_name'];?> </a> </h3>
            </div>
          </div>
          </div>
          
          <div class="inner-box">
            <div class="welcome-msg">
            <div id="accordion" class="panel-group">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title"> <a href="#collapseB1"  data-toggle="collapse"> My details </a> </h4>
                </div>
                <div class="panel-collapse collapse in" id="collapseB1">
                  <div class="panel-body">
                    <form class="form-horizontal" role="form" action="<?php echo base_url();?>user/update_account/<?=$record_list[0]['id'];?>" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                      <div class="form-group">
                        <?php if ($this->session->flashdata('message')!=null) {?>
                                <div class=" col-sm-offset-3 col-md-9" id="infoMessage" style="font-size: 15px;padding: 3px; color: green;font-family: sans-serif; "><?php echo $this->session->flashdata('message');?></div>
                        <?php } ?>
                        </div>
                       
                      <div class="form-group">
                        <label  class="col-sm-3 control-label">First Name</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" required  name="first_name" value="<?=$record_list[0]['first_name'];?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label  class="col-sm-3 control-label">Last name</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" required name="last_name" value="<?=$record_list[0]['last_name'];?>">
                        </div>
                      </div>
                       <div class="form-group">
                        <label class="col-sm-3 control-label">Change Profile</label>
                        <div class="col-sm-9">
                           <input type="file" name="image" id="image" value="Upload Image">
                           <span style=" color: red; ">(max image size <?=$max_size?>KB with max width <?=$max_width?>px and max height <?=$max_height?>px )</span>
                       </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-3 control-label">Date of Birth</label>
                        <div class="col-sm-9">
                           <input type="text" class="form-control date-only" name ="dob" value="<?=$record_list[0]['dob'];?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label  class="col-sm-3 control-label">Zip Code</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control"  name="zipcode" value="<?=$record_list[0]['zipcode'];?>">
                        </div>
                      </div>
                        <div class="form-group">
                        <label  class="col-sm-3 control-label">Country</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control"  name="country" value="<?=$record_list[0]['country'];?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label  class="col-sm-3 control-label">Address</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" autocomplete="off" id="address"  name="address" value="<?=$record_list[0]['address'];?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-3 control-label">Vehicle Type</label>
                            <div class="col-sm-9">
                              <select name ="v_type" id="v_type" class="form-control" style=" font-size: 13px; ">
                                <?php if ($record_list[0]['v_type']=="cts"){ ?>
                                    <option value="">Select</option> 
                                    <option value="cts" selected>Car Truck & SUVs</option> 
                                    <option value="cht">Commercial Heavy trucs</option> 
                                    <option value="rv">RV</option>
                                    <option value="mc">MotorCycle</option>
                                    <option value="bc">Bicycle</option>
                                <?php } else if ($record_list[0]['v_type']=="cht") { ?>
                                    <option value="">Select</option> 
                                    <option value="cts" >Car Truck & SUVs</option> 
                                    <option value="cht" selected>Commercial Heavy trucs</option> 
                                    <option value="rv">RV</option>
                                    <option value="mc">MotorCycle</option>
                                    <option value="bc">Bicycle</option>
                                    <option value="garage">Garage</option>
                                <?php } else if ($record_list[0]['v_type']=="rv") { ?>
                                    <option value="">Select</option> 
                                    <option value="cts" >Car Truck & SUVs</option> 
                                    <option value="cht">Commercial Heavy trucs</option> 
                                    <option value="rv" selected>RV</option>
                                    <option value="mc">MotorCycle</option>
                                    <option value="bc">Bicycle</option>
                                 <?php } else if ($record_list[0]['v_type']=="mc") { ?>
                                    <option value="">Select</option> 
                                    <option value="cts" >Car Truck & SUVs</option> 
                                    <option value="cht">Commercial Heavy trucs</option> 
                                    <option value="rv">RV</option>
                                    <option value="mc" selected>MotorCycle</option>
                                    <option value="bc">Bicycle</option>
                                 <?php } else if ($record_list[0]['v_type']=="bc") { ?>
                                    <option value="">Select</option> 
                                    <option value="cts" >Car Truck & SUVs</option> 
                                    <option value="cht">Commercial Heavy trucs</option> 
                                    <option value="rv">RV</option>
                                    <option value="mc">MotorCycle</option>
                                    <option value="bc" selected>Bicycle</option>
                                 <?php } else { ?>
                                    <option value="">Select</option> 
                                    <option value="cts">Car Truck & SUVs</option> 
                                    <option value="cht">Commercial Heavy trucs</option> 
                                    <option value="rv">RV</option>
                                    <option value="mc">MotorCycle</option>
                                    <option value="bc">Bicycle</option>
                                 <?php } ?>
                           </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="Phone" class="col-sm-3 control-label">Vehicle brand</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control"  name="v_brand" value="<?=$record_list[0]['v_brand'];?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="Phone" class="col-sm-3 control-label">Vehicle plate</label>
                        <div class="col-sm-9">
                         <?php if ($record_list[0]['v_type']=="bc") { ?>
                             <input disabled type="text" class="form-control"  name="v_plate" value="">
                         <?php } else { ?>
                             <input type="text" autocomplete="off" class="form-control" id="v_plate"  name="v_plate" value="<?=$record_list[0]['v_plate'];?>">
                         <?php } ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9"> </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                          <button type="submit" class="btn btn-default">Update</button>
                        </div>
                      </div>
                       <input type="hidden"  name ="himage" id ="himage" value="<?=$record_list[0]['image'];?>">  
                    </form>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title"> <a href="#collapseB2"  data-toggle="collapse"> Change Password </a> </h4>
                </div>
                <div class="panel-collapse collapse" id="collapseB2">
                  <div class="panel-body">
                    <form class="form-horizontal" role="form" action="<?php echo base_url();?>user/update_password/<?=$record_list[0]['id'];?>" method="post" accept-charset="utf-8">
                     
                      <div class="form-group">
                        <label  class="col-sm-3 control-label">New Password</label>
                        <div class="col-sm-9">
                          <input type="password" class="form-control"  placeholder="" name = "password" id = "password"> 
                        </div>
                         
                      </div>
                     
                      <div class="form-group">
                        <label  class="col-sm-3 control-label">Confirm Password</label>
                        <div class="col-sm-9">
                          <input type="password" class="form-control"  placeholder=""  name = "password_confirm" id = "password_confirm">
                        </div>
                       
                      </div>
                      <div class="form-group">
                           <div class="col-sm-offset-3 col-sm-9">
                            <div id="error" style = "display: none;color: red;"></div>
                           </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                          <button type="submit" class="btn btn-default">Update</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            <!--/.row-box End--> 
            
          </div>
        </div>
        <!--/.page-content--> 
      </div>
      <!--/.row--> 
    </div>
    <!--/.container--> 
  </div>
  <!-- <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
/.main-container -->
  <?php include ("partial/afooter.php"); ?>
<script src="http://maps.google.com/maps/api/js?libraries=places" 
          type="text/javascript"></script>
  <script>
  $(function() {
   // $( ".date-only" ).datepicker();
    $('#password').on( "focusout", function(e) {
            var password = $("#password").val();
            if(password.length < 8){
                     $("#error").html("* Password must contain over 8 characters!");
                     $("#error").css('display', 'block');
            }
            else{
                $("#error").css('display', 'none');
            }
           
     });
    $('#password_confirm').on( "focusout", function(e) {
            var password = $("#password").val();
            var confirmPassword = $("#password_confirm").val();

                    if (password != confirmPassword){
                            $("#error").html("* Confirn passwords do not match!");
                            $("#error").css('display', 'block');

                    }
                    else{
                            $("#error").css('display', 'none');

                    }

    });
    $('#v_type').on('change', function(e) {
         
         
         

          if ( $(this).val() == "bc") {
              $('#v_plate').val("");
            $('#v_plate').attr('disabled',true );
          } else {
            $('#v_plate').attr('disabled', false);
          }
      });
      var input = document.getElementById('address'); 
     var autocomplete = new google.maps.places.Autocomplete(input);
     google.maps.event.addListener(autocomplete, 'place_changed', function () {
     });
});
  </script>
