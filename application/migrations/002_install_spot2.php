<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Install_Spot2 extends CI_Migration {

	public function up()
	{
		
		// Table structure for table 'users_groups'
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'int',
				'constraint' => '11',
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'spot_available_services' => array(
				'type' => 'VARCHAR',
				'constraint' => '250',
				'null' => TRUE
			)
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('spot_available_services');
             
                // Table spot_type
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'int',
				'constraint' => '11',
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'spot_type' => array(
				'type' => 'VARCHAR',
				'constraint' => '250',
				'null' => TRUE
			)
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('spot_type');
                
                
                // Table spot_type
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'int',
				'constraint' => '11',
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'period' => array(
				'type' => 'VARCHAR',
				'constraint' => '250',
				'null' => TRUE
			),'min' => array(
				'type' => 'VARCHAR',
				'constraint' => '250',
				'null' => TRUE
			)  ,'max' => array(
				'type' => 'VARCHAR',
				'constraint' => '250',
				'null' => TRUE
			)                  
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('spot_price');
      
      
      

	}

	public function down()
	{
		$this->dbforge->drop_table('spot_available_services', TRUE);
                $this->dbforge->drop_table('spot_type', TRUE);
                $this->dbforge->drop_table('spot_price', TRUE);
        }
}
