<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Install_Spot4 extends CI_Migration {

	public function up()
	{
		
		
                $this->dbforge->drop_column('spot', 'minHP');
                $this->dbforge->drop_column('spot', 'maxHP');
                $this->dbforge->drop_column('spot', 'minDP');
                $this->dbforge->drop_column('spot', 'maxDP');
                $this->dbforge->drop_column('spot', 'minWP');
                $this->dbforge->drop_column('spot', 'maxWP');
                $this->dbforge->drop_column('spot', 'minMP');
                $this->dbforge->drop_column('spot', 'maxMP');
                     
                
                $fields = array(
                       'hourlyPrice' => array(
				'type' => 'VARCHAR',
				'constraint' => '250',
                                'null' => TRUE
			),
			'dailyPrice' => array(
				'type' => 'VARCHAR',
				'constraint' => '250',
                                'null' => TRUE
			),
			'weeklyPrice' => array(
				'type' => 'VARCHAR',
				'constraint' => '250',
                                 'null' => TRUE
			),
			'monthlyPrice' => array(
				'type' => 'VARCHAR',
				'constraint' => '250',
                                'null' => TRUE
			)
                );
                $this->dbforge->add_column('spot', $fields);
      
      

	}

	
}
