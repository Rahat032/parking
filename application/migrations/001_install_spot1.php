<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Install_Spot1 extends CI_Migration {

	public function up()
	{
		// Drop table 'groups' if it exists
		//$this->dbforge->drop_table('groups', TRUE);

		// Table structure for table 'groups'
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'MEDIUMINT',
				'constraint' => '8',
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'name' => array(
				'type' => 'VARCHAR',
				'constraint' => '20',
			),
			'description' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
			)
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('groups');

		// Dumping data for table 'groups'
		$data = array(
			array(
				'id' => '1',
				'name' => 'admin',
				'description' => 'Administrator'
			),
			array(
				'id' => '2',
				'name' => 'members',
				'description' => 'General User'
			)
		);
		$this->db->insert_batch('groups', $data);


		// Drop table 'users' if it exists
		//$this->dbforge->drop_table('users', TRUE);

		// Table structure for table 'users'
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'MEDIUMINT',
				'constraint' => '8',
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
                        'ip_address' => array(
				'type' => 'VARCHAR',
				'constraint' => '16'
			),
			'password' => array(
				'type' => 'VARCHAR',
				'constraint' => '80',
			),
                        'salt' => array(
				'type' => 'VARCHAR',
				'constraint' => '40'
			),
			'email' => array(
				'type' => 'VARCHAR',
				'constraint' => '100'
			),
			'activation_code' => array(
				'type' => 'VARCHAR',
				'constraint' => '40',
				'null' => TRUE
			),
			'forgotten_password_code' => array(
				'type' => 'VARCHAR',
				'constraint' => '40',
				'null' => TRUE
			),
			'forgotten_password_time' => array(
				'type' => 'INT',
				'constraint' => '11',
				'unsigned' => TRUE,
				'null' => TRUE
			),
			'remember_code' => array(
				'type' => 'VARCHAR',
				'constraint' => '40',
				'null' => TRUE
			),
			'created_on' => array(
				'type' => 'INT',
				'constraint' => '11',
				'unsigned' => TRUE,
			),
			'last_login' => array(
				'type' => 'INT',
				'constraint' => '11',
				'unsigned' => TRUE,
				'null' => TRUE
			),
			'active' => array(
				'type' => 'TINYINT',
				'constraint' => '1',
				'unsigned' => TRUE,
				'null' => TRUE
			),
			'first_name' => array(
				'type' => 'VARCHAR',
				'constraint' => '50',
				'null' => TRUE
			),
			'last_name' => array(
				'type' => 'VARCHAR',
				'constraint' => '50',
				'null' => TRUE
			),
			'phone' => array(
				'type' => 'VARCHAR',
				'constraint' => '20',
				'null' => TRUE
			),
			'address' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
				'null' => TRUE
			),
			'country' => array(
				'type' => 'VARCHAR',
				'constraint' => '50',
				'null' => TRUE
			),
                        'zipcode' => array(
                                    'type' => 'VARCHAR',
                                    'constraint' => '50',
                                    'null' => TRUE
                        ),
                        'dob' => array(
                                    'type' => 'Date',
                                    'null' => TRUE
                        ),
                        'image' => array(
				'type' => 'VARCHAR',
				'constraint' => '250',
				'null' => TRUE
			),
                        
                        'v_type' => array(
                                    'type' => 'VARCHAR',
                                    'constraint' => '100',
                                    'null' => TRUE
                        ),
                        'v_plate' => array(
                                    'type' => 'VARCHAR',
                                    'constraint' => '100',
                                    'null' => TRUE
                        ),
                        'v_brand' => array(
                                    'type' => 'VARCHAR',
                                    'constraint' => '100',
                                    'null' => TRUE
                        ),
			'gender' => array(
				'type' => 'INT',
				'constraint' => '5',
				'null' => TRUE
			)

		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('users');

		

		// Drop table 'users_groups' if it exists
		//$this->dbforge->drop_table('users_groups', TRUE);

		// Table structure for table 'users_groups'
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'MEDIUMINT',
				'constraint' => '8',
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'user_id' => array(
				'type' => 'MEDIUMINT',
				'constraint' => '8',
				'unsigned' => TRUE
			),
			'group_id' => array(
				'type' => 'MEDIUMINT',
				'constraint' => '8',
				'unsigned' => TRUE
			)
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('users_groups');
                    
		// Dumping data for table 'users_groups'
		$data = array(
			array(
				'id' => '1',
				'user_id' => '1',
				'group_id' => '1',
			),
			array(
				'id' => '2',
				'user_id' => '1',
				'group_id' => '2',
			)
		);
		$this->db->insert_batch('users_groups', $data);


		// Drop table 'login_attempts' if it exists
		//$this->dbforge->drop_table('login_attempts', TRUE);

		// Table structure for table 'login_attempts'
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'MEDIUMINT',
				'constraint' => '8',
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'ip_address' => array(
				'type' => 'VARCHAR',
				'constraint' => '16'
			),
			'login' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
				'null', TRUE
			),
			'time' => array(
				'type' => 'INT',
				'constraint' => '11',
				'unsigned' => TRUE,
				'null' => TRUE
			)
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('login_attempts');
                
                // Drop table 'spot' if it exists
		//$this->dbforge->drop_table('spot', TRUE);

		// Table structure for table 'users'
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'MEDIUMINT',
				'constraint' => '8',
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
                        'user_id' => array(
				'type' => 'INT',
				'constraint' => '11'
			),
			'street' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
                            'null' => TRUE
			),
			'zip' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
                            'null' => TRUE
			),
			'city' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
                            'null' => TRUE
			),
			'state' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
                                'null' => TRUE
			),
			'country' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
                                 'null' => TRUE
			),
                        'spot_discription' => array(
				'type' => 'VARCHAR',
				'constraint' => '250',
                                 'null' => TRUE
			),
			'image' => array(
				'type' => 'VARCHAR',
				'constraint' => '250',
				'null' => TRUE
			),
			'spot_type' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
				'null' => TRUE
			),
			'spot_available_service' => array(
				'type' => 'VARCHAR',
				'constraint' => '250',
				'null' => TRUE
                            
			),
			'price' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
				'null' => TRUE
			),
			'dateFrom' => array(
				'type' => 'DATE',
				'unsigned' => TRUE,
				'null' => TRUE
			),
			'dateTo' => array(
				'type' => 'DATE',
				
				'unsigned' => TRUE,
				'null' => TRUE
			),'latitude' => array(
				 'type' => 'DECIMAL',
                                 'constraint' => '10,6',
                                
				'null' => TRUE
			),
                        'longitude' => array(
				  'type' => 'DECIMAL',
                                  'constraint' => '10,6',
				  'null' => TRUE
			),
                        'active' => array(
				'type' => 'TINYINT',
				'constraint' => '1',
				'unsigned' => TRUE,
				'null' => TRUE
			)
			

		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('spot');
           

	}

	public function down()
	{
		$this->dbforge->drop_table('users', TRUE);
                $this->dbforge->drop_table('spot', TRUE);
               
		$this->dbforge->drop_table('groups', TRUE);
		$this->dbforge->drop_table('users_groups', TRUE);
		$this->dbforge->drop_table('login_attempts', TRUE);
	}
}
