<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Install_Spot5 extends CI_Migration {

	public function up()
	{
		
		
               $fields = array(
                        'From' => array(
                                'name' => 'dateFrom',
                                'type' => 'DATE',
				'unsigned' => TRUE,
				'null' => TRUE
                                
                        ),
                        'To' => array(
                                'name' => 'dateTo',
                                'type' => 'DATE',
				'unsigned' => TRUE,
				'null' => TRUE
                           
                        )
                );
                $this->dbforge->modify_column('spot', $fields);
      
      

	}

	
}
