<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Install_Spot8 extends CI_Migration {

	public function up()
	{
		// Drop table 'groups' if it exists
		//$this->dbforge->drop_table('groups', TRUE);

		// Table structure for table 'groups'
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'MEDIUMINT',
				'constraint' => '8',
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'spot_id' => array(
				'type' => 'int',
				'constraint' => '11',
				'unsigned' => TRUE
			),
			'booking_by_id' => array(
				'type' => 'int',
				'constraint' => '11',
				'unsigned' => TRUE
			),
                        'spot_type' => array(
				'type' => 'VARCHAR',
                                'constraint' => '250',
				 'null' => TRUE
			),
                        'dateTimeFrom' => array(
				'type' => 'datetime',
				 'null' => TRUE
			),
                        'dateTimeTo' => array(
				'type' => 'datetime',
				 'null' => TRUE
			),
                        'nextAvailable' => array(
				'type' => 'datetime',
				 'null' => TRUE
			)
                        
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('spot_booking');

		
	}

	public function down()
	{
		$this->dbforge->drop_table('spot_booking', TRUE);
        }
}
