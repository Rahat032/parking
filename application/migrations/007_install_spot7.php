<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Install_Spot7 extends CI_Migration {

    public function up() {

        $fields = array(
            'spot_discription' => array(
                'name' => 'spot_discription',
                'type' => 'TEXT',
            ),
        );
        $this->dbforge->modify_column('spot', $fields);
    }

}
