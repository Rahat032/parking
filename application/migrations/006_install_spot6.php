<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Install_Spot6 extends CI_Migration {

	public function up()
	{
		
		
                $this->dbforge->drop_column('spot', 'street');
                $this->dbforge->drop_column('spot', 'zip');
                $this->dbforge->drop_column('spot', 'city');
                $this->dbforge->drop_column('spot', 'state');
                $this->dbforge->drop_column('spot', 'country');
            
                //spot_discription     
             
                $fields = array(
                       'address' => array(
				'type' => 'VARCHAR',
				'constraint' => '1000',
                                'null' => TRUE
			)
                );
                $this->dbforge->add_column('spot', $fields);
      
      

	}

	
}
