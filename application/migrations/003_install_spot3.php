<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Install_Spot3 extends CI_Migration {

	public function up()
	{
		
		
                $this->dbforge->drop_column('spot', 'price');
                $fields = array(
                       'minHP' => array(
				'type' => 'VARCHAR',
				'constraint' => '250',
                                'null' => TRUE
			),
			'maxHP' => array(
				'type' => 'VARCHAR',
				'constraint' => '250',
                                'null' => TRUE
			),
			'minDP' => array(
				'type' => 'VARCHAR',
				'constraint' => '250',
                                 'null' => TRUE
			),
			'maxDP' => array(
				'type' => 'VARCHAR',
				'constraint' => '250',
                                'null' => TRUE
			),
			'minWP' => array(
				'type' => 'VARCHAR',
				'constraint' => '250',
                                 'null' => TRUE
			),
                        'maxWP' => array(
				'type' => 'VARCHAR',
				'constraint' => '250',
                                 'null' => TRUE
			),
                        'minMP' => array(
				'type' => 'VARCHAR',
				'constraint' => '250',
                                 'null' => TRUE
			),
                        'maxMP' => array(
				'type' => 'VARCHAR',
				'constraint' => '250',
                                 'null' => TRUE
			)
                );
                $this->dbforge->add_column('spot', $fields);
      
      

	}

	
}
