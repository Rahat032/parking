

$(document).ready(function() {
  $('.date-only').datetimepicker({
  	timepicker:false,
  	format:'Y-m-d'
  });
  $('.time-only').datetimepicker({
  	datepicker:false,
  	format:'H:i'
  });
  $('.date-time').datetimepicker({
    format:'Y-m-d H:i'
  });
  $('.pmol-time-only').datetimepicker({
    datepicker:false,
    format:'H:i',
    allowTimes:[
    '00:00', '01:00', '02:00','03:00', '04:00', '05:00', '06:00', '07:00','08:00', '09:00', '10:00', '11:00', '12:00'
    ]
  });
});
